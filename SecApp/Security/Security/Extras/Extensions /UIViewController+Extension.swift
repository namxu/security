//
//  UIViewController+Extension.swift
//  iKioska
//
//  Created by Jassie on 28/09/2017.
//  Copyright © 2017 Jassie. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    class func controllerFromSB()->UIViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        return storyBoard.instantiateViewController(withIdentifier: String(describing: self))
    }
    
    
    
    func hideNavigationBar(){
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    func showNavigationBar() {
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
}



@IBDesignable
class GradientButton:UIButton {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var leftGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()//setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    @IBInspectable
    var rightGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()
            //setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    override class var layerClass: AnyClass{
        get{
            return CAGradientLayer.self
        }
    }
    
    private func updateView(){
        let layer = self.layer as! CAGradientLayer
        layer.colors = [leftGradientColor.cgColor, rightGradientColor.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0.5)
        layer.endPoint = CGPoint(x: 1, y: 0.5)
        layer.locations = [0,1]
    }
    
    
}

@IBDesignable
class GradientTopBottomView: UIView {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()//setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()
            //setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    override class var layerClass: AnyClass{
        get{
            return CAGradientLayer.self
        }
    }
    
    private func updateView(){
        let layer = self.layer as! CAGradientLayer
        layer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 0, y: 0.80)
        layer.locations = [0.5]
    }
    
}

@IBDesignable
class GradientLeftRightView: UIView {
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var leftGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()//setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    @IBInspectable
    var rightGradientColor: UIColor = UIColor.clear {
        didSet {
            updateView()
            //setGradient(leftGradientColor: leftGradientColor, rightGradientColor: rightGradientColor)
        }
    }
    
    override class var layerClass: AnyClass{
        get{
            return CAGradientLayer.self
        }
    }
    
    private func updateView(){
        let layer = self.layer as! CAGradientLayer
        layer.colors = [leftGradientColor.cgColor, rightGradientColor.cgColor]
        layer.startPoint = CGPoint(x: 0, y: 0.5)
        layer.endPoint = CGPoint(x: 1, y: 0.5)
        layer.locations = [0,1]
    }
    
    func animateGradient(duration: TimeInterval, newLeftColor: UIColor, newRightColor: UIColor) {
        let fromColors = self.gradientLayer.colors
        let toColors: [CGColor] = [ newLeftColor.cgColor, newRightColor.cgColor]
        self.gradientLayer.colors = toColors
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        animation.fromValue = fromColors
        animation.toValue = toColors
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = .forwards
        //        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        self.gradientLayer.add(animation, forKey:"colorChange")
    }
    
    private func setGradient(leftGradientColor: UIColor?, rightGradientColor: UIColor?) {
        if let leftGradientColor = leftGradientColor, let rightGradientColor = rightGradientColor {
            gradientLayer.frame = bounds
            gradientLayer.colors = [leftGradientColor.cgColor, rightGradientColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
    }
}
