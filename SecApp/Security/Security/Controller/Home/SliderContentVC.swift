//
//  SliderContentVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 19/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

import Foundation
import UIKit

class SliderContentVC: BaseViewController{
    
    private let tableView = UITableView()
    private let tableViewIdentifier = "TABLE_VIEW_IDENTIFIER"
    private var items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)]
    var baseVc:BaseViewController? = nil
    var selectedState :TabsValue = .ACTIVE
    var selectedAdminState:TabsAdminValue = .ACTIVE
    var currentStat :(TabsValue,TabsAdminValue) {
        set {
            self.selectedState = newValue.0
            self.selectedAdminState = newValue.1
            self.checkState()
            self.tableView.reloadData()
        }
        get {
            return (self.selectedState,self.selectedAdminState)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func checkState(){
        if(DataManager.sharedInstance.isAdmin()){
            switch selectedAdminState {
            case .ACTIVE: items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)]
            case .Assigned: items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
                ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
                ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)]
            case .NotAssigned:
                items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
                    ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
                    ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)
                    ,StateItem("complete1","Have an eye on suspicious activity in Food Cafe.",.COMPLETE,.COMPLETE)
                    ,StateItem("complete3","Note susoicious activities in SuperStore.",.COMPLETE,.COMPLETE)]
            
            }
        }else{
            switch  selectedState {
                
            case .ACTIVE:
                items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)]
                break
            case .LastDays:
                items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
                    ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
                    ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)
                    ,StateItem("complete1","Have an eye on suspicious activity in Food Cafe.",.COMPLETE,.COMPLETE)
                    ,StateItem("complete3","Note susoicious activities in SuperStore.",.COMPLETE,.COMPLETE)]
                break
                
            case .LastHours:
                items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)]
            }
        }
        
    }
    
    private func setupUI(){
        // view
        view.addSubview(tableView)
        
        // tableView
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 8).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -28).isActive = true
        tableView.tableFooterView = UIView()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: tableViewIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib.init(nibName: StatesCell.identifier, bundle: nil), forCellReuseIdentifier: StatesCell.identifier)
        tableView.allowsSelection = true
        
    }
    
}

extension SliderContentVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.getVC(storyboard: .MAIN,vcIdentifier: DescriptionVC.identifier) as! DescriptionVC
        self.baseVc?.pushVC(vc)
    }
    
}

extension SliderContentVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StatesCell.identifier, for: indexPath) as? StatesCell
        cell?.configure(items[indexPath.item])
        cell?.selectionStyle = .none
        return cell!
    }
    
}

enum TabsValue:String,Codable,CaseIterable {
    case ACTIVE = "Today"
    case LastHours = "Last 24 Hours"
    case LastDays = "Next 30 Days"
//    case ALL = "All"
}

enum TabsAdminValue:String,Codable,CaseIterable {
    case ACTIVE = "Today"
    case Assigned = "Last 24 Hours"
    case NotAssigned = "Next 30 Days"
//    case ALL = "All"
}
