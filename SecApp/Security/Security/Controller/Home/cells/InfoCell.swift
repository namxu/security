//
//  InfoCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 19/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class InfoCell: UICollectionViewCell {

    static let identifier = "InfoCell"
    
    @IBOutlet weak var lblValue:UILabel!
    @IBOutlet weak var ivImage:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
