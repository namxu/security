//
//  HolidayVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 02/05/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class HolidayVC: BaseViewController {

    static let identifier = "HolidayVC"
    @IBOutlet weak var tvDecription:UITextView!
    @IBOutlet weak var tfDate:UITextView!
    var startDate:Date? = nil
    var endDate:Date? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvDecription.delegate = self
        self.tvDecription.placeholder = "Tap to enter text..."
        self.tvDecription.placeholderColorTV = UIColor.darkGray.withAlphaComponent(0.5)
        self.tfDate.placeholder = ""
        self.tfDate.placeholderColorTV = UIColor.darkGray.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickCalendar(_ sender:UIButton){
        self.openCalendar(delegate: self)
    }

    @IBAction func onClickDone(_ sender:UIButton){
        if(!self.tfDate.text!.isEmpty){
            self.onClickDismiss(sender)
        }else{
            if(self.tfDate.text!.isEmpty){
                self.showToast(text: "Dates Required!", type: .error, location: .top)
            }
        }
    }
}

extension HolidayVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == self.tvDecription {
            if(textView.text!.count > 150){
                textView.text = "\(textView.text.prefix(150))"
            }
            guard let completetext = textView.text else {
                return true
            }
            
            let newLength = completetext.count + text.count - range.length
            if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = newLength > 0
            }
            return newLength <= 150
        }
        return true
    }
    
   
    
}


extension HolidayVC: CalendarDateRangePickerViewControllerDelegate{
    func didCancelPickingDateRange() {
        
    }
    
    func didPickDateRange(startDate: Date!, endDate: Date!) {
        if(startDate != nil && endDate != nil){
            self.startDate = startDate
            self.endDate = endDate
            self.tfDate.text = "\(startDate.getDateFormat(outPutFormat: "EE dd-MMM-yyyy")) to \(endDate.getDateFormat(outPutFormat: "EE dd-MMM-yyyy"))"
            
        }

    }
    
    
}
