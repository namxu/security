////
////  AppleLoginManager.swift
////  Trecco
////
////  Created by Jassie on 12/11/2019.
////  Copyright © 2019 Jhony. All rights reserved.
////
//
//import Foundation
//import AuthenticationServices
//
//@available(iOS 13.0, *)
//protocol AppleLoginDelegate {
//    func appleLoginSuccess(user : User)
//    func appleLoginFailed(error : String)
//}
//
//@available(iOS 13.0, *)
//class AppleLoginManager: NSObject, ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
//    static let sharedInstance = AppleLoginManager()
//    var delegate : AppleLoginDelegate?
//    var access_token : String  = ""
//    var appleDelegateController:UIViewController?
//    
//    func loginApple(appleDelegate : UIViewController) {
//        appleDelegateController = appleDelegate
//        delegate = (appleDelegate as! AppleLoginDelegate)
//        handleAuthorizationAppleID()
//    }
//    
//    @objc func handleAuthorizationAppleID() {
//        let request = ASAuthorizationAppleIDProvider().createRequest()
//        request.requestedScopes = [.fullName, .email]
//        let controller = ASAuthorizationController(authorizationRequests: [request])
//        controller.delegate = self
//        controller.presentationContextProvider = self
//        controller.performRequests()
//    }
//    
//    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//        return appleDelegateController!.view.window!
//    }
//    
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        //Handle error here
//        delegate?.appleLoginFailed(error: error.localizedDescription)
//    }
//    
//    // ASAuthorizationControllerDelegate function for successful authorization
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
//            // Create an account in your system.
//            let userIdentifier = appleIDCredential.user
//            print(userIdentifier)
//            let appleIDProvider = ASAuthorizationAppleIDProvider()
//            appleIDProvider.getCredentialState(forUserID: userIdentifier) { (credentialState, error) in
//                switch credentialState {
//                case .authorized:
//                    // The Apple ID credential is valid. Show Home UI Here
//                    break
//                case .revoked:
//                    // The Apple ID credential is revoked. Show SignIn UI Here.
//                    break
//                case .notFound:
//                    // No credential was found. Show SignIn UI Here.
//                    break
//                default:
//                    break
//                }
//            }
//            let userFirstName = appleIDCredential.fullName?.givenName
//            print(userFirstName as Any)
//            let userLastName = appleIDCredential.fullName?.familyName
//            print(userLastName as Any)
//            let userEmail = appleIDCredential.email
//            print(userEmail as Any)
//            //Navigate to other view controller
//            delegate?.appleLoginSuccess(user: User().genrateUserFromApple(appleUser: appleIDCredential))
//
//        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
//            // Sign in using an existing iCloud Keychain credential.
//            let username = passwordCredential.user
//            print(username as Any)
//            let password = passwordCredential.password
//            print(password as Any)
//            
//            //delegate?.appleLoginSuccess(user: User().genrateUserFromApple(appleUser: appleIDCredential))
//            
//            //Navigate to other view controller
//        }
//    }
//    
//    
//    private func performExistingAccountSetupFlows() {
//        // Prepare requests for both Apple ID and password providers.
//        let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
//        
//        // Create an authorization controller with the given requests.
//        let authorizationController = ASAuthorizationController(authorizationRequests: requests)
//        authorizationController.delegate = (appleDelegateController as! ASAuthorizationControllerDelegate)
//        authorizationController.presentationContextProvider = (appleDelegateController as! ASAuthorizationControllerPresentationContextProviding)
//        authorizationController.performRequests()
//    }
//    
//}
