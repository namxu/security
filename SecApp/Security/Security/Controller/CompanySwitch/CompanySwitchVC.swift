//
//  CompanySwitchVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 30/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class CompanySwitchVC: BaseViewController {

    static let identifier = "CompanySwitchVC"
    @IBOutlet weak var tableView:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTBL()
        
    }
    

}

extension CompanySwitchVC :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Company.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SwitchCompanyCell.identifier, for: indexPath) as! SwitchCompanyCell
        cell.configure(title: Company.allCases[indexPath.item].rawValue, isSelected: DataManager.sharedInstance.getIndexSelected() == indexPath.item)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DataManager.sharedInstance.setupSwitchCompanySelection(index:indexPath.item)
        self.onClickDismiss(UIButton())
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.1
    }
    
    private func setupTBL(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib.init(nibName: SwitchCompanyCell.identifier, bundle: nil), forCellReuseIdentifier: SwitchCompanyCell.identifier)
        self.tableView.contentInset = UIEdgeInsets.init(top: CGFloat(8), left: CGFloat(0), bottom: CGFloat(8), right: CGFloat(0))

    }
    
}

enum Company:String,CaseIterable {
    case Company1 = "Abc Security pvt. ltd."
    case Company2 = "Pentagon Security Agency"
    case Company3 = "Bravo Security Company"
    case Company4 = "Rapid Action Security"
    case Company5 = "Blink Security Services"
}
