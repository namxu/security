//
//  MessageVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 02/05/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class MessageVC: BaseViewController {

    static let identifier = "MessageVC"
    @IBOutlet weak var tvDecription:UITextView!
    @IBOutlet weak var tvSubject:UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvDecription.delegate = self
        self.tvDecription.placeholder = "Tap to enter text..."
        self.tvDecription.placeholderColorTV = UIColor.darkGray.withAlphaComponent(0.5)
        self.tvSubject.placeholder = ""
        self.tvSubject.placeholderColorTV = UIColor.darkGray.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickDone(_ sender:UIButton){
        self.onClickDismiss(sender)
    }
    
}
extension MessageVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == self.tvDecription {
            if(textView.text!.count > 150){
                textView.text = "\(textView.text.prefix(150))"
            }
            guard let completetext = textView.text else {
                return true
            }
            
            let newLength = completetext.count + text.count - range.length
            if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = newLength > 0
            }
            return newLength <= 150
        }
        return true
    }
    
   
    
}
