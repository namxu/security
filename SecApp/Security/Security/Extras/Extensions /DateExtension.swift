//
//  DateExtension.swift
//  StarNote
//
//  Created by MACBOOK on 28/02/2019.
//  Copyright © 2019 MAC MINI 3. All rights reserved.
//

import UIKit
//import GoogleMaps
extension Date {
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    func futureDateDisplay() -> String {

        let calendar = Calendar.current
        let minutesUntil = calendar.date(byAdding: .minute, value: 1, to: Date())!
        let hourUntil = calendar.date(byAdding: .hour, value: 1, to: Date())!
        let dayUntil = calendar.date(byAdding: .day, value: 1, to: Date())!
        let weekUntil = calendar.date(byAdding: .day, value: 7, to: Date())!

        if minutesUntil > self {
            let diff = Calendar.current.dateComponents([.second], from: Date(), to: self).second ?? 0
            return "\(diff) sec from now on"
        } else if hourUntil > self {
            let diff = Calendar.current.dateComponents([.minute], from: Date(), to: self).minute ?? 0
            return "\(diff) min from now on"
        } else if dayUntil > self {
            let diff = Calendar.current.dateComponents([.hour], from: Date(), to: self).hour ?? 0
            return "\(diff) hrs from now on"
        } else if weekUntil > self {
            let diff = Calendar.current.dateComponents([.day], from: Date(), to: self).day ?? 0
            return "\(diff) days from now on"
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: Date(), to: self).weekOfYear ?? 0
        return "\(diff) weeks from now on"
    }
    func timeAgoDisplay() -> String {
        
        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!
        
        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff) " + (diff == 1 ? "sec ago" : "secs ago")
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff) " + (diff == 1 ? "min ago" : "mins ago")
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff) " + (diff == 1 ? "hour ago" : "hours ago")
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff) " + (diff == 1 ? "day ago" : "days ago")
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
        return "\(diff) " + (diff == 1 ? "week ago" : "weeks ago")
    }
    
}



//extension GMSMarker {
//    func setIconSize(scaledToSize newSize: CGSize) {
//        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
//        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
//        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        icon = newImage
//    }
//}

extension String{
    
}
