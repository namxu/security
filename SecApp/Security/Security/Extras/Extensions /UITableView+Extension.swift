//
//  UITableView+Extension.swift
//  Trecco
//
//  Created by Jaisee on 11/14/19.
//  Copyright © 2019 Jhony. All rights reserved.
//

import Foundation
import UIKit
import MapKit
extension UITableView{
    func setEmptyMessage(text : String = "No data available") {
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        noDataLabel.text          = text
        noDataLabel.font = UIFont(name: "Avenir-Heavy", size: 16.0)!
        noDataLabel.textColor     = BeboColor.beboGreenColor.withAlphaComponent(0.7)
        noDataLabel.textAlignment = .center
        self.backgroundView  = noDataLabel
        self.separatorStyle  = .none
    }
    
    
    func removeEmptyMessage()  {
        self.backgroundView = nil
    }
}



extension CLLocation {
    
    func fetchCityAndCountry(completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.country, $1) }
    }
    func fetchCityAndCountryState(completion: @escaping (_ city: String?, _ country:  String?, _ state:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(self) { completion($0?.first?.locality, $0?.first?.country,$0?.first?.administrativeArea, $1) }
    }
}
