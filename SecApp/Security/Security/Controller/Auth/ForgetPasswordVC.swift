//
//  ForgetPasswordVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 13/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ForgetPasswordVC: BaseViewController {
    static let identifier = "ForgetPasswordVC"
    @IBOutlet weak var tfEmail:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickCodeSend(_ sender:UIButton){
        if(self.tfEmail.text!.isValidEmail()){
            let vc = self.getVC(storyboard: .MAIN, vcIdentifier: ForgetPasswordVerificationSignInVC.identifier) as! ForgetPasswordVerificationSignInVC
            vc.email = self.tfEmail.text!
            self.pushVC(vc)
        }else{
            if(!self.tfEmail.text!.isValidEmail()){
                self.showToast(text: "Invalid Email!", type: .error, location: .top)
            }
        }
    }
    
}

