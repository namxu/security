//
//  SliderTblCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 22/08/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SliderTblCell: UICollectionViewCell {

    static let identifier = "SliderTblCell"
    @IBOutlet weak var tableView:UITableView!
    var items = [StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupTbl()
    }

    var openDetailVC:(() -> Void)?
    
}
extension SliderTblCell:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openDetailVC?()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StatesCell.identifier, for: indexPath) as? StatesCell
        cell?.configure(items[indexPath.item])
        cell?.selectionStyle = .none
        return cell!
    }
    
    func setupTbl(){
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "TABLE_VIEW_IDENTIFIER")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib.init(nibName: StatesCell.identifier, bundle: nil), forCellReuseIdentifier: StatesCell.identifier)
        tableView.allowsSelection = true
    }
    
}
