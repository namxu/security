//
//  HomeVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 18/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit
import SwiftyPickerPopover

class HomeVC: BaseViewController {
    
    @IBOutlet weak var segmentView: TwicketSegmentedControl!
    var segementAdminSelected:SegementAdminOption = SegementAdminOption.Today
    var segementUserSelected:SegementUserOption = SegementUserOption.Today
    static let identifier = "HomeVC"
    var itemsList = [[StateItem]]()
    @IBOutlet weak var ivProfile:UIImageView!
    @IBOutlet weak var ivProfileSideMenu:UIImageView!
    @IBOutlet weak var uiWholeSideMenu:UIView!
    @IBOutlet weak var uiViewContent:UIView!
    @IBOutlet weak var uiViewSider:UIView!
    @IBOutlet weak var uiViewSiderInner:UIView!
    @IBOutlet weak var uiViewInfo:UIView!
    @IBOutlet weak var uiSideMenu:UIView!
    @IBOutlet weak var tblSideMenu:UITableView!
    @IBOutlet weak var collectionView:UICollectionView!
    //MARK: Info Outlets
    @IBOutlet weak var lblBJobs :UILabel!
    @IBOutlet weak var lblBHours :UILabel!
    @IBOutlet weak var lblBComments :UILabel!
    @IBOutlet weak var lblBEarning :UILabel!
    @IBOutlet weak var ivBJobs :UIImageView!
    @IBOutlet weak var ivBHours :UIImageView!
    @IBOutlet weak var ivBComments :UIImageView!
    @IBOutlet weak var ivBEarning :UIImageView!
    
    @IBOutlet weak var uiForCompany:UIStackView!
    //
    @IBOutlet weak var uiBtmLine1:UIView!
    @IBOutlet weak var uiBtmLine2:UIView!
//    @IBOutlet weak var uiBtmLine3:UIView!
    @IBOutlet weak var uiBtmLine4:UIView!
    @IBOutlet weak var uiBtmLine5:UIView!
    
    @IBOutlet weak var uiComingSoon:UIView!
    //
    
    private var slidingTabController:UISimpleSlidingTabController!// = UISimpleSlidingTabController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiWholeSideMenu.isHidden = true
        self.hideSideMenu()
        self.setupTBL()
        self.slidingTabController = UISimpleSlidingTabController.init()
        self.slidingTabController.baseVc = self
        self.setupInfoValues()
        let button = UIButton()
        button.tag = 3
        self.onClickBottomTabs(button)
        self.setupSegmentView()
        self.uiForCompany.isHidden = !DataManager.sharedInstance.isAdmin()
        // Do any additional setup after loading the view.
    }
    
    
    func showSideMenu(){
        UIView.animate(withDuration: TimeInterval(0.5), animations: {
            self.uiSideMenu.frame.origin.x = 0
            self.uiWholeSideMenu.isHidden = false
        }, completion: { (isDone) in
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
        })
    }
    
    func hideSideMenu(){
        UIView.animate(withDuration: TimeInterval(0.5), animations: {
            self.uiSideMenu.frame.origin.x = -self.uiSideMenu.frame.width
        }, completion: { (isDone) in
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            self.uiWholeSideMenu.isHidden = true
        })
    }
    
    @IBAction func onClickEmergency(_ sender:UIButton){
        self.makeACall("+923337394353")
    }
    
    @IBAction func onClickPhone(_ sender:UIButton){
        self.makeACall("+923337394353")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.ivProfile.cornerRadius = self.ivProfile.frame.height/2
//        self.setupSLiderVC()
        self.setupInfoValues()
        self.setupCL()
    }
    
    override func viewDidLayoutSubviews() {
        self.ivProfileSideMenu.makeCircle = true
    }
    
    
    @IBAction func onClickOptions(_ sender:UIButton){
        StringPickerPopover(title: "", choices: ["Company A","Company B"])
            .setSelectedRow(0)
            .setDoneButton(action: { (popover, selectedRow, selectedString) in
                print("done row \(selectedRow) \(selectedString)")
                self.lblTitleHeader.text = "\(selectedString)"
            })
            .setCancelButton(action: { (_, _, _) in print("cancel")}
        )
            .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func onClickMenuDismiss(_ sender:UIButton){
        self.hideSideMenu()
    }
    
    @IBAction func onClickMenu(_ sender:UIButton){
        self.showSideMenu()
    }
    
    func makeACall(_ number:String) {
        guard let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) else { return }
        if #available(iOS 10, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func setupSLiderVC(){
        if(self.slidingTabController.titles.count > 0){
            return Void()
        }
        self.slidingTabController.view.frame.width = self.uiViewSiderInner.frame.width
        self.slidingTabController.view.frame.height = self.uiViewSiderInner.frame.height
        self.uiViewSiderInner.addSubview(slidingTabController.view) // add slidingTabController to main view
        if DataManager.sharedInstance.isAdmin() {
            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsAdminValue.ACTIVE.rawValue) // add first item
            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsAdminValue.Assigned.rawValue) // add second item
            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsAdminValue.NotAssigned.rawValue) // add third item
//            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsAdminValue.ALL.rawValue) // add third item
        }else{
            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsValue.ACTIVE.rawValue) // add first item
//            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsValue.LastHours.rawValue) // add second item
            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsValue.LastDays.rawValue) // add third item
//            self.slidingTabController.addItem(item: SliderContentVC(), title: TabsValue.ALL.rawValue) // add third item
        }
        self.slidingTabController.setHeaderActiveColor(color: .white) // default blue
        self.slidingTabController.setHeaderInActiveColor(color: .black) // default gray
        self.slidingTabController.setHeaderBackgroundColor(color: .white) // default white
        self.slidingTabController.setHeaderLine(color: .red)
        self.slidingTabController.setCurrentPosition(position: 0) // default 0
        self.slidingTabController.setStyle(style: .fixed) // default fixed
        self.slidingTabController.build() // build
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    @IBAction func onClickPanic(_ sd:UIButton){
        let vc = self.getVC(storyboard: .MAIN, vcIdentifier: PanicVC.identifier)
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        self.presentVC(vc)
    }
    
    func setupInfoValues(){
//        print("Font is : \(self.lblBJobs.font)")
        if !DataManager.sharedInstance.isAdmin() {
            
            setAttributedTextForLabel(mainString: "12\n\(InfoCase.allCases[0].rawValue)", attributedStringsArray: ["12",InfoCase.allCases[0].rawValue], lbl: self.lblBJobs, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "100:20\n\(InfoCase.allCases[1].rawValue)", attributedStringsArray: ["100:20",InfoCase.allCases[1].rawValue], lbl: self.lblBHours, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "150\n\(InfoCase.allCases[2].rawValue)", attributedStringsArray: ["150",InfoCase.allCases[2].rawValue], lbl: self.lblBComments, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "200$\n\(InfoCase.allCases[3].rawValue)", attributedStringsArray: ["200$",InfoCase.allCases[3].rawValue], lbl: self.lblBEarning, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            
            self.ivBJobs.image = #imageLiteral(resourceName: "Free Guards") //UIImage.init(named: InfoCase.allCases[0].rawValue)
            self.ivBHours.image = #imageLiteral(resourceName: "Number of Hours") //UIImage.init(named: InfoCase.allCases[1].rawValue)
            self.ivBComments.image = #imageLiteral(resourceName: "On Duty Guards") //UIImage.init(named: InfoCase.allCases[2].rawValue)
            self.ivBEarning.image = #imageLiteral(resourceName: "Number of Guard") //UIImage.init(named: InfoCase.allCases[3].rawValue)
        }else{
            setAttributedTextForLabel(mainString: "90\n\(InfoAdminCase.allCases[0].rawValue)", attributedStringsArray: ["90",InfoAdminCase.allCases[0].rawValue], lbl: self.lblBJobs, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "80\n\(InfoAdminCase.allCases[1].rawValue)", attributedStringsArray: ["80",InfoAdminCase.allCases[1].rawValue], lbl: self.lblBHours, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "60\n\(InfoAdminCase.allCases[2].rawValue)", attributedStringsArray: ["60",InfoAdminCase.allCases[2].rawValue], lbl: self.lblBComments, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            setAttributedTextForLabel(mainString: "20\n\(InfoAdminCase.allCases[3].rawValue)", attributedStringsArray: ["20",InfoAdminCase.allCases[3].rawValue], lbl: self.lblBEarning, color: [UIColor.black,UIColor.black], attFont: [BeboFont.bold(CGFloat(16)).font(),BeboFont.medium(CGFloat(13)).font()])
            
//            self.ivBJobs.image = UIImage.init(named: InfoAdminCase.allCases[0].rawValue)
//            self.ivBHours.image = UIImage.init(named: InfoAdminCase.allCases[1].rawValue)
//            self.ivBComments.image = UIImage.init(named: InfoAdminCase.allCases[2].rawValue)
//            self.ivBEarning.image = UIImage.init(named: InfoAdminCase.allCases[3].rawValue)
            self.ivBJobs.image = #imageLiteral(resourceName: "Free Guards") //UIImage.init(named: InfoCase.allCases[0].rawValue)
            self.ivBHours.image = #imageLiteral(resourceName: "Number of Hours") //UIImage.init(named: InfoCase.allCases[1].rawValue)
            self.ivBComments.image = #imageLiteral(resourceName: "On Duty Guards") //UIImage.init(named: InfoCase.allCases[2].rawValue)
            self.ivBEarning.image = #imageLiteral(resourceName: "Number of Guard") //UIImage.init(named: InfoCase.allCases[3].rawValue)
        }
    }

    @IBAction func onClickBottomTabs(_ sd:UIButton){
        switch sd.tag {
        case 1 :
            self.uiBtmLine1.isHidden = false
            self.uiBtmLine2.isHidden = true
//            self.uiBtmLine3.isHidden = true
            self.uiBtmLine4.isHidden = true
            self.uiBtmLine5.isHidden = true
            self.uiComingSoon.isHidden = false
            break
        case 2 :
            self.uiBtmLine1.isHidden = true
            self.uiBtmLine2.isHidden = false
//            self.uiBtmLine3.isHidden = true
            self.uiBtmLine4.isHidden = true
            self.uiBtmLine5.isHidden = true
            self.uiComingSoon.isHidden = false
            break
        case 3 :
            self.uiBtmLine1.isHidden = true
            self.uiBtmLine2.isHidden = true
//            self.uiBtmLine3.isHidden = true
            self.uiBtmLine4.isHidden = true
            self.uiBtmLine5.isHidden = true
            self.uiComingSoon.isHidden = true
            break
        case 4 :
            self.uiBtmLine1.isHidden = true
            self.uiBtmLine2.isHidden = true
//            self.uiBtmLine3.isHidden = true
            self.uiBtmLine4.isHidden = false
            self.uiBtmLine5.isHidden = true
            self.uiComingSoon.isHidden = false
            break
        case 5 :
            self.uiBtmLine1.isHidden = true
            self.uiBtmLine2.isHidden = true
//            self.uiBtmLine3.isHidden = true
            self.uiBtmLine4.isHidden = true
            self.uiBtmLine5.isHidden = true
            self.uiComingSoon.isHidden = true
            let vc = self.getVC(storyboard: .MAIN, vcIdentifier: HolidayVC.identifier)
            self.presentVC(vc)
            break
        default : break
        }
    }
    
    
    func setupSegmentView(){
        var titles = [String]()
        if(DataManager.sharedInstance.isAdmin()){
            titles = [SegementAdminOption.Today.rawValue,SegementAdminOption.Last24Hours.rawValue,SegementAdminOption.Next30Days.rawValue]
            self.itemsList = [[StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)],[StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
            ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
            ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)],[StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
            ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
            ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)
            ,StateItem("complete1","Have an eye on suspicious activity in Food Cafe.",.COMPLETE,.COMPLETE)
                ,StateItem("complete3","Note susoicious activities in SuperStore.",.COMPLETE,.COMPLETE)]]
        }else{
            titles = [SegementAdminOption.Today.rawValue,SegementAdminOption.Next30Days.rawValue]
            self.itemsList = [[StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)],[StateItem.init("authBg","To make regular rounds of Hotel.",.ACTIVE,.ACTIVE)
            ,StateItem.init("tempPerson","Establishing a sufficient degree of security in Software company.",.PENDING,.PENDING)
            ,StateItem.init("complete2","Have an eye on suspicious activity and unauthorized persons.",.COMPLETE,.COMPLETE)
            ,StateItem("complete1","Have an eye on suspicious activity in Food Cafe.",.COMPLETE,.COMPLETE)
            ,StateItem("complete3","Note susoicious activities in SuperStore.",.COMPLETE,.COMPLETE)]]
        }
        self.segmentView.setSegmentItems(titles)
        self.segmentView.delegate = self
        self.segmentView.font = BeboFont.bold(CGFloat(14)).font()//FrenzyFont.kNunitoBold(16).font()//UIFont.init(name:
        self.segmentView.highlightTextColor = .black
        self.segmentView.sliderBackgroundColor = .white//FrenzyColor.redColor
        self.segmentView.defaultTextColor = .white//FrenzyColor.segmentUnSelectColor
        self.segmentView.isSliderShadowHidden = true
        self.segmentView.segmentsBackgroundColor = .white
    }
    
}


extension HomeVC : TwicketSegmentedControlDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataManager.sharedInstance.isAdmin() ? SegementAdminOption.allCases.count : SegementUserOption.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderTblCell.identifier, for: indexPath) as! SliderTblCell
        cell.items = self.itemsList[indexPath.item]
        cell.tableView.reloadData()
        cell.openDetailVC = {
            let vc = self.getVC(storyboard: .MAIN,vcIdentifier: DescriptionVC.identifier) as! DescriptionVC
            self.pushVC(vc)
        }
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        self.segmentView.move(to: page)
//        self.callNext(self.pageControl.currentPage)
    }
    func didSelect(_ segmentIndex: Int) {
        self.collectionView.scrollToItem(at: IndexPath.init(row: segmentIndex, section:
            0), at: .centeredHorizontally, animated: true)
    }
    
    private func setupCL(){
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.setCollectionViewLayout(flowLayout, animated: true)
        self.collectionView.isPagingEnabled = true
        self.collectionView.register(UINib.init(nibName: SliderTblCell.identifier, bundle: nil), forCellWithReuseIdentifier: SliderTblCell.identifier)
    }
    
    var flowLayout: UICollectionViewFlowLayout {
        let cellSize = CGSize(width:self.collectionView?.frame.size.width ?? CGFloat(50), height:self.collectionView?.frame.size.height ?? CGFloat(50))
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 0.0
        layout.minimumInteritemSpacing = 0.0
        return layout
    }
    
    
}


extension HomeVC :UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.sharedInstance.isAdmin() ? SideAdminMenuCase.allCases.count : SideMenuCase.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuCell.identifier, for: indexPath) as! SideMenuCell
        cell.lblTitle.text = DataManager.sharedInstance.isAdmin() ? SideAdminMenuCase.allCases[indexPath.item].rawValue : SideMenuCase.allCases[indexPath.item].rawValue
        cell.ivImage.image = UIImage.init(named: DataManager.sharedInstance.isAdmin() ? SideAdminMenuCaseImages.allCases[indexPath.item].rawValue : SideMenuCaseImages.allCases[indexPath.item].rawValue)
        cell.ivImage.tintColor = UIColor.darkGray.withAlphaComponent(1)
        cell.lblTitle.textColor = cell.ivImage.tintColor
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / CGFloat((DataManager.sharedInstance.isAdmin() ? (SideAdminMenuCase.allCases.count + 5) : (SideMenuCase.allCases.count + 4)))//6
    }
    
    private func setupTBL(){
        self.tblSideMenu.delegate = self
        self.tblSideMenu.dataSource = self
        self.tblSideMenu.register(UINib.init(nibName: SideMenuCell.identifier, bundle: nil), forCellReuseIdentifier: SideMenuCell.identifier)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hideSideMenu()
        if(DataManager.sharedInstance.isAdmin()){
            switch SideAdminMenuCase.allCases[indexPath.item] {
                
            case .MESSAGE:
                let vc = self.getVC(storyboard: .MAIN, vcIdentifier: MessageVC.identifier)
                self.presentVC(vc)
            case .NOTIFICATION:
                break
            case .HELP:
                break
            case .LOGOUT:
                let choose = UIAlertController(title: "Log Out", message: "Do you want to logout?", preferredStyle: .actionSheet)
                let yes = UIAlertAction.init(title: "Yes", style: .destructive) { (handler) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                let no = UIAlertAction.init(title: "No", style: .cancel) { (handler) in
                    
                }
                choose.addAction(yes)
                choose.addAction(no)
                self.present(choose, animated: true, completion: nil)
            }
        }else{
            switch SideMenuCase.allCases[indexPath.item] {
                
            case .SWITCH_COMPANY:
                let vc = self.getVC(storyboard: .MAIN, vcIdentifier: CompanySwitchVC.identifier)
                self.presentVC(vc)
                break
            case .MESSAGE:
                let vc = self.getVC(storyboard: .MAIN, vcIdentifier: MessageVC.identifier)
                self.presentVC(vc)
                break
            case .NOTIFICATION:
                break
            case .LEAVE:
                let vc = self.getVC(storyboard: .MAIN, vcIdentifier: HolidayVC.identifier)
                self.presentVC(vc)
                break
            case .HELP:
                break
            case .LOGOUT:
                let choose = UIAlertController(title: "Log Out", message: "Do you want to logout?", preferredStyle: .actionSheet)
                let yes = UIAlertAction.init(title: "Yes", style: .destructive) { (handler) in
                    self.navigationController?.popToRootViewController(animated: true)
                }
                let no = UIAlertAction.init(title: "No", style: .cancel) { (handler) in
                    
                }
                choose.addAction(yes)
                choose.addAction(no)
                self.present(choose, animated: true, completion: nil)

                break
                
            case .SUMMERY:
                let vc = self.getVC(storyboard: .MAIN, vcIdentifier: SummeryVC.identifier)
                self.presentVC(vc)
            }
        }
    }
    
    
}

enum SegementAdminOption :String,CaseIterable{
    
    case Today = "Today"
    case Last24Hours = "Last 24 Hours"
    case Next30Days = "Next 30 Days"
}

enum SegementUserOption :String,CaseIterable{
    case Today = "Today"
    case Next30Days = "Next 30 Days"
}

enum SideMenuCase:String,CaseIterable {
    case SWITCH_COMPANY = "Switch Company"
    case MESSAGE = "Message"
    case NOTIFICATION = "Notification"
    case LEAVE = "Holidays"
    case HELP = "Help"
    case SUMMERY = "Summary"
    case LOGOUT = "LogOut"
}

enum SideAdminMenuCase:String,CaseIterable {
    
    case MESSAGE = "Message"
    case NOTIFICATION = "Notification"
    
    case HELP = "Help"
    case LOGOUT = "LogOut"
}

enum SideAdminMenuCaseImages:String,CaseIterable {

    case MESSAGE = "Message"
    case NOTIFICATION = "bell"
    case HELP = "Help"
    case LOGOUT = "LogOut"
}

enum SideMenuCaseImages:String,CaseIterable {
    case SWITCH_COMPANY = "Switch Company"
    case MESSAGE = "Message"
    case NOTIFICATION = "bell"
    case LEAVE = "Leave"
    case HELP = "Help"
    case SUMMERY = "Report"
    case LOGOUT = "LogOut"
}

enum InfoCase:String,CaseIterable {
    case NUMBER_OF_JOBS = "Total Jobs / Month"
    case NUMBER_OF_HOURS = "Total Hours / Month"
    case NOTIFICATION = "Active Jobs"
    case TOTAL_EARNING = "Total Guards"
    
}

enum InfoAdminCase:String,CaseIterable {
    case NUMBER_OF_GUARD = "Number of Guard"
    case AVABILABLE_GUARD = "Available Guard"
    case ON_DUTY_GUARDS = "On Duty Guards"
    case FREE_GUARDS = "Free Guards"
    
}
