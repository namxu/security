//
//  Enums.swift
//  NightOut
//
//  Created by user on 21/01/2020.
//  Copyright © 2020 CodingPixel. All rights reserved.
//

import Foundation

public enum CustomError: Error {
    case networkNotAvailable
    case somethingWrong
}

extension CustomError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .networkNotAvailable:
            return NSLocalizedString("Network is not available. Please Connect to the internet", comment: "network not available")
        case .somethingWrong:
            return NSLocalizedString("Something went wrong", comment: "Something is wrong")
        }
    }
}


public enum NotificationType: String {
    case addFriend = "addFriend"
    case singleChat = "singleChat"
    case partyChat = "partyChat"
    case addParty = "addParty"
    case sosAlert = "SOS"
}

public enum NotificationText: String {
    case addFriend = "added you in friend list."
    case singleChat = "sent you message."
    case partyChat = "sent message in party."
    case addParty = "added you in party."
}
