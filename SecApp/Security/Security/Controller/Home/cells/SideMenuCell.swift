//
//  SideMenuCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 18/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    static let identifier = "SideMenuCell"
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var ivImage:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
