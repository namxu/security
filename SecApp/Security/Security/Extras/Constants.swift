//
//  Constants.swift
//  NightOut
//
//  Created by CP on 12/20/19.
//  Copyright © 2019 CodingPixel. All rights reserved.
//

import UIKit
import SystemConfiguration
import GoogleMaps
let kError = "Error"
let kOkay = "Okay"
let kSuccess = "Success"
let kSuccessDone = "Successfully done."
let kIdentifierLocationPicker = "LocationPicker"
let kResponseTimeFormat = "yyyy-MM-dd HH:mm:ss"
class Constants {
    static var regUserId = "0"
    static var regEmail = ""
    static var regPassword = ""
    static let kAuthorize = "Authorize"
    static let placesKey = "AIzaSyBfRJPn3O6lg2WY4iC1CvWA7234AFF3S9dDAeW1g"
    static let GOOGLE_CLIENT_ID = "524887844074-iai2b3uen3sm0c681phjsr6gipgqb3h3.apps.googleusercontent.com"
     //"com.googleusercontent.apps.1010551799484-ji9nf8b44shjrnbo9rbci7tlachlck2m"
    //duet; "AIzaSyBfRJPn3O6lg2WY4iC1CvWA73S9dDAeW1g"
    static let kResponseTimeFormat = "yyyy-MM-dd HH:mm:ss"
    static let kMilesInMerter = "6000" // 9000 mean 5
    static let oneSignalAppId = "1b00b87a-a015-48bd-99a4-c1d185b395d0"
    static let chatIdKey = "chat_id"
    static let isPartyKey = "is_party"
    static let kPrefKeyUser = "pref_user"
    static let chatNameKey = "chat_name"
    static let kMaxRadius  : CGFloat = 200
    static let kMaxDuration : TimeInterval = 4
    static let profileSetting = [
        ["name":"Account Setting", "icon":"account_setting","des":"Name, Gender, Adress, Buy VIP…"],
        ["name":"Booking Management", "icon":"booking","des":"Manage your booking"],
        ["name":"Loyalty Point Management", "icon":"loyaltty","des":"50 Point"],
        ["name":"Voucher Management", "icon":"voucher","des":"Your Voucher"],
        ["name":"Services & Pricing", "icon":"services","des":"Our Services & Pricing"]]
    static let kStoryBoardMain = "Main"
    static let kStoryBoardAuth = "Auth"
    static let kStoryBoardProfile = "Profile"
    static let kStoryBoardBookBoard = "BookBoard"
    static let kX_API_KEY = "X-API-KEY"
    static let kAuthorization = "Authorization"
    static let kServerError = "Server Error"
    static let kAccept = "Accept"
    static let kApplicationJson = "application/json"
    static let GoogleMapsAPIServerKey = ""
}

// MARK: API BASE PATH , SOCKET PATH
internal struct APIConstants {
    static let SocketURL = "https://www.duet.icu:5002"
    private static let baseIp = "thetesting.uk"// Live one is https://gentsoftown.london/api
//    private static let baseIp = "gentsoftown.london/api"
    static let BasePath = "https://\(baseIp)/barbershop"
    static let BasePathDeepLink = "https://\(baseIp)/barbershop"
    static let imageBasePath = "https://\(baseIp)/barbershop/uploads/"
    static let apiSecret = "AvHVGhSgeUMiLloF4zFOA2UYkEOFaRu5"
    static let googlePhotosHost = "https://maps.googleapis.com/maps/api/place/photo"
    static func googlePhotoURL(photoReference:String, maxWidth:Int) -> URL? {
        return URL.init(string: "\(googlePhotosHost)?maxwidth=\(maxWidth)&key=\(Constants.GoogleMapsAPIServerKey)&photoreference=\(photoReference)")
    }

    static func shareDeepLink(id:String,type:String)-> String{
        return BasePathDeepLink + "details?id=\(id)&type=\(type)"
    }
    static func getImagePath(name:String) -> URL{
        return URL.init(string: "\(imageBasePath)users/\(name)")!
    }
    static func getBarberImagePath(name:String) -> URL{
        return URL.init(string: "\(imageBasePath)barber/actual_images/\(name)")!
    }
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}
public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

enum Storyboards:String {
    case AUTH = "Auth"
    case MAIN = "Main"
    case PROFILE = "Profile"
    case BOOKBOARD = "BookBoard"
    func board() ->String { return self.rawValue }
}

struct BeboColor{
    static let beboRedColor = UIColor.init(hexColor: "EE4266")
    static let beboBlackColor = UIColor.init(hexColor: "1E2023")
    static let beboLightGrayColor = UIColor.init(hexColor: "9FACBD")
    static let beboBlueColor = UIColor.init(hexColor: "1281E3")
    static let beboLightBgColor = UIColor.init(hexColor: "313444")
    static let beboGreenColor = UIColor.init(hexColor: "e9af27")//UIColor.init(hexColor: "00A9A5")
    static let beboDarkBgColor = UIColor.init(hexColor: "1A1B25")
    static let beboLightAlphaColor = UIColor.init(hexColor: "2C2E3D")
    static let beboLightUnSelectColor = UIColor.init(hexColor: "3B3F52")
    static let beboDarkerBgColor = UIColor.init(hexColor: "1C202A")
    
}

public enum BeboFont {
    
    case reguler(CGFloat)
    case bold(CGFloat)
    case semiBold(CGFloat)
    case black(CGFloat)
    case light(CGFloat)
    case extraLight(CGFloat)
    case extraBold(CGFloat)
    case medium(CGFloat)
    case thin(CGFloat)
    case book(CGFloat)
    func font() -> UIFont {
        switch self {
        case .reguler(let size) :
            return UIFont(name: "Poppins-Regular", size: size)!
        case .bold(let size) :
            return UIFont(name: "Gotham-Bold", size: size)!
        case .semiBold(let size) :
            return UIFont(name: "Poppins-SemiBold", size: size)!
        case .extraBold(let size) :
            return UIFont(name: "Poppins-ExtraBold", size: size)!
        case .black(let size) :
            return UIFont(name: "Gotham-Black", size: size)!
        case .light(let size) :
            return UIFont(name: "Gotham-XLight", size: size)!
        case .extraLight(let size) :
            return UIFont(name: "Poppins-ExtraLight", size: size)!
        case .medium(let size) :
            return UIFont(name: "Gotham Medium", size: size)!
        case .thin(let size) :
            return UIFont(name: "Poppins-Thin", size: size)!
        case .book(let size) :
            return UIFont(name: "Gotham-Book", size: size)!
        }
    }
}

let stateName : [String] = ["Alabama",
                            "Alaska",
                            "American Samoa",
                            "Arizona",
                            "Arkansas",
                            "California",
                            "Colorado",
                            "Connecticut",
                            "Delaware",
                            "District Of Columbia",
                            "Federated States Of Micronesia",
                            "Florida",
                            "Georgia",
                            "Guam",
                            "Hawaii",
                            "Idaho",
                            "Illinois",
                            "Indiana",
                            "Iowa",
                            "Kansas",
                            "Kentucky",
                            "Louisiana",
                            "Maine",
                            "Marshall Islands",
                            "Maryland",
                            "Massachusetts",
                            "Michigan",
                            "Minnesota",
                            "Mississippi",
                            "Missouri",
                            "Montana",
                            "Nebraska",
                            "Nevada",
                            "New Hampshire",
                            "New Jersey",
                            "New Mexico",
                            "New York",
                            "North Carolina",
                            "North Dakota",
                            "Northern Mariana Islands",
                            "Ohio",
                            "Oklahoma",
                            "Oregon",
                            "Palau",
                            "Pennsylvania",
                            "Puerto Rico",
                            "Rhode Island",
                            "South Carolina",
                            "South Dakota",
                            "Tennessee",
                            "Texas",
                            "Utah",
                            "Vermont",
                            "Virgin Islands",
                            "Virginia",
                            "Washington",
                            "West Virginia",
                            "Wisconsin",
                            "Wyoming"]

extension Date {
    func getDateForMat(outPutFormat:String = "dd")-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = outPutFormat
        return dateFormatter.string(from: self)
//        return  "18-10-2018"
    }
}

enum IphoneType {
    case IPHONE_5_5S_5C
    case IPHONE_6_6S_7_8
    case IPHONE_6PLUS_6SPLUS_7PLUS_8PLUS
    case IPHONE_X_XS_11_Pro
    case IPHONE_XS_Max_11_Pro_Max
    case IPHONE_XR_11
    case UNKNOWN
}

func checkIphoneIs() -> IphoneType{
    if UIDevice().userInterfaceIdiom == .phone {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            return .IPHONE_5_5S_5C
        case 1334:
            return .IPHONE_6_6S_7_8
        case 1920, 2208:
            return .IPHONE_6PLUS_6SPLUS_7PLUS_8PLUS
        case 2436:
            return .IPHONE_X_XS_11_Pro
        case 2688:
            return .IPHONE_XS_Max_11_Pro_Max
        case 1792:
            return .IPHONE_XR_11
        default:
            return .UNKNOWN
        }
    }else{
        return .UNKNOWN
    }
}



import UIKit

class DataManager: NSObject {
    //
//    var user: User?{
//        set{
//            self.saveUserPermanentally(newValue)
//        }
//        get{
//            self.getPermanentlySavedUser()
//        }
//    }
    //    var user: User? {
    //        didSet {
    //            saveUserPermanentally()
    //        }
    //    }
//    func saveUserPermanentally(_ item:User?) {
//        if item != nil {
//            let encodedData = try? JSONEncoder().encode(item)
//            UserDefaults.standard.set(encodedData, forKey: "user")
//        }
//    }
//
//    func getPermanentlySavedUser() -> User? {
//        if let data = UserDefaults.standard.data(forKey: "user"),
//            let userData = try? JSONDecoder().decode(User.self, from: data) {
//            return userData
//        } else {
//            return nil
//        }
//    }
//
    var deviceToken:String = UIDevice.current.identifierForVendor!.uuidString
    
    static let sharedInstance = DataManager()
    
    func logoutUser() {
//        user = nil
        self.resetDefaults()
        UserDefaults.standard.removeObject(forKey: "user")
    }
    
    func isSetupComplete() -> Bool {
        return UserDefaults.standard.bool(forKey: "is_remember")
    }
    
    func isAdmin() -> Bool {
        return UserDefaults.standard.bool(forKey: "isAdminLogin")
    }
    
    func setIsAdmin(_ value:Bool){
        UserDefaults.standard.set(value, forKey: "isAdminLogin")
        UserDefaults.standard.synchronize()
    }
    
    func setIsRememberLogin(_ value:Bool){
        UserDefaults.standard.set(value, forKey: "isRememberLogin")
        UserDefaults.standard.synchronize()
    }
    func isRememberLogin() -> Bool {
        return UserDefaults.standard.bool(forKey: "isRememberLogin")
    }
    
    func setupComplete(remember : Bool) {
        UserDefaults.standard.set(remember, forKey: "is_remember")
        UserDefaults.standard.synchronize()
    }
    
    
    func isFriendToturialShow() -> Bool {
        return UserDefaults.standard.bool(forKey: "isFriendToturialShow")
    }
    
    func setFriendToturialShow(remember : Bool) {
        UserDefaults.standard.set(remember, forKey: "isFriendToturialShow")
        UserDefaults.standard.synchronize()
    }
    
    func setupCityFilter(_ remember : String) {
        UserDefaults.standard.set(remember, forKey: "FilterCity")//(remember, forKey: "is_remember")
        UserDefaults.standard.synchronize()
    }
    //
    func setupFirstLogin(_ remember : Bool) {
        UserDefaults.standard.set(remember, forKey: "isFirstLogin")//(remember, forKey: "is_remember")
        UserDefaults.standard.synchronize()
    }
    
    func setupSwitchCompanySelection(index:Int){
        UserDefaults.standard.set(index, forKey: "indexSelectedSC")
        UserDefaults.standard.synchronize()
    }
    func getIndexSelected() -> Int {
        return UserDefaults.standard.integer(forKey: "indexSelectedSC")
    }
    
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
//        let isSkip = self.isSkipTrue()
//        let id = self.getPermanentlySavedUser()?.id
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}



/// Extend UITextView and implemented UITextViewDelegate to listen for changes
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    @IBInspectable
    public var placeholderColorTV: UIColor? {
        get {
            var placeholderColorTV: UIColor?
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderColorTV = placeholderLabel.textColor
            }
            
            return placeholderColorTV
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.textColor = newValue
            } else {
                self.addPlaceholder("",newValue!)
            }
        }
    }
    
    /// The UITextView placeholder text
    @IBInspectable
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String = "", _ color:UIColor = .white) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = color.withAlphaComponent(0.5)
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
    
    
    
}



extension Date {
    
    // Convert local time to UTC (or GMT)
    
    
    // Convert UTC (or GMT) to local time
    
    func getDateFormat(outPutFormat:String = "MM / dd / yyyy",isLocal:Bool = false) -> String {
        let formatter = DateFormatter();
        formatter.dateFormat = outPutFormat
        let mnth_name =  formatter.string(from: isLocal ? self.toLocalTime() : self)
        return mnth_name
    }
    
}



extension GMSMarker {
    func setIconSize(scaledToSize newSize: CGSize) {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        icon?.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        icon = newImage
    }
}

