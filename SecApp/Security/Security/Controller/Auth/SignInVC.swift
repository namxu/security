//
//  SignInVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 13/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SignInVC: BaseViewController {
    
    static let identifier = "SignInVC"
    @IBOutlet weak var tfEmail:UITextField!
    @IBOutlet weak var tfPassord:UITextField!
    @IBOutlet weak var uiCheck:UIView!
    var tickBox : Checkbox?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(DataManager.sharedInstance.isRememberLogin()){
            let vc = self.getVC(storyboard: .MAIN, vcIdentifier: HomeVC.identifier)
            self.pushVC(vc)
        }
        self.setupCheckBox()
    }
    
    func setupCheckBox(){
        // tick
        tickBox = Checkbox.init(frame: CGRect.init(x: 0, y: 0, width: uiCheck.frame.size.width, height: uiCheck.frame.size.height))
        tickBox?.borderStyle = .square
        tickBox?.checkmarkStyle = .tick
        tickBox?.checkedBorderColor = UIColor.darkGray//init(hexColor: "BA232C")
        tickBox?.uncheckedBorderColor = .lightGray
        tickBox?.checkboxFillColor = .clear//UIColor.darkGray//UIColor.init(hexColor: "BA232C")//.clear
        tickBox?.checkmarkColor = UIColor.darkGray//.init(hexColor: "BA232C")//.clear
        tickBox?.checkmarkSize = 0.7
        tickBox?.valueChanged = { (value) in
            print("tickBox value change: \(value)")
            _ = value ? (self.tickBox?.checkboxFillColor == UIColor.init(hexColor: "BA232C")) : (self.tickBox?.checkboxFillColor == .clear)
        }
        self.uiCheck.addSubview(tickBox!)
        self.uiCheck.cornerRadius = 2
        self.uiCheck.clipsToBounds = true
    }
    
    @IBAction func onClickCheckBox(_ sender:UIButton){
        tickBox?.isChecked = !(tickBox?.isChecked ?? false)
        _ = (tickBox?.isChecked ?? false) ? (self.uiCheck.backgroundColor == UIColor.init(hexColor: "BA232C")) : (self.uiCheck.backgroundColor == .clear)
        print("IsTicke \(tickBox?.isChecked ?? false)")
    }
    
    @IBAction func onClickSignIn(_ sender:UIButton){
        if(self.tfEmail.text!.isValidEmail() && !self.tfPassord.text!.isEmpty){
            //HomeVC
            let vc = self.getVC(storyboard: .MAIN, vcIdentifier: HomeVC.identifier)
            if(self.tfEmail.text!.trimmingCharacters(in: .whitespacesAndNewlines) == "admin@gmail.com"){
                DataManager.sharedInstance.setIsAdmin(true)
            }else{
                DataManager.sharedInstance.setIsAdmin(false)
            }
            DataManager.sharedInstance.setIsRememberLogin(self.tickBox?.isChecked ?? false)
            self.pushVC(vc)
        }else{
            if(!self.tfEmail.text!.isValidEmail()){
                self.showToast(text: "Invalid Email!", type: .error, location: .top)
                return
            }
            if(self.tfPassord.text!.isEmpty){
                self.showToast(text: "Password Required!", type: .error, location: .top)
                return
            }
        }

    }
    
    @IBAction func onClickForgotPassword(_ sender:UIButton){
        let vc = self.getVC(storyboard: .MAIN, vcIdentifier: ForgetPasswordVC.identifier)
        self.pushVC(vc)
    }
    
}

