//
//  SummeryDescriptionCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 22/08/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SummeryDescriptionCell: UITableViewCell {

    static let identifier = "SummeryDescriptionCell"
    
    @IBOutlet weak var lblHour:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
