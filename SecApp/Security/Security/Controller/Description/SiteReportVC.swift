//
//  SiteReportVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 02/05/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SiteReportVC: BaseViewController {

    static let identifier = "SiteReportVC"
    @IBOutlet weak var tvDecription:UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tvDecription.delegate = self
        self.tvDecription.placeholder = "Tap to enter text..."
        self.tvDecription.placeholderColorTV = UIColor.darkGray.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }
        
    @IBAction func onClickDone(_ sender:UIButton){
        self.onClickDismiss(sender)
    }


}
extension SiteReportVC : UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == self.tvDecription {
            if(textView.text!.count > 150){
                textView.text = "\(textView.text.prefix(150))"
            }
            guard let completetext = textView.text else {
                return true
            }
            
            let newLength = completetext.count + text.count - range.length
            if let placeholderLabel = textView.viewWithTag(100) as? UILabel {
                placeholderLabel.isHidden = newLength > 0
            }
            return newLength <= 150
        }
        return true
    }
    
   
    
}
