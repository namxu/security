////
////  GoogleLoginManager.swift
////  Trecco
////
////  Created by Jassie on 11/12/19.
////  Copyright © 2019 Jhony. All rights reserved.
////
//
//import Foundation
//import GoogleSignIn
//@available(iOS 13.0, *)
//protocol GoogleLoginDelegate {
//    func googleLoginSuccess(user : User)
//    func googleLoginFailed(error : String)
//}
//@available(iOS 13.0, *)
//class GoogleLoginManager: NSObject {
//    static let sharedInstance = GoogleLoginManager()
//    var delegate : GoogleLoginDelegate?
//    var access_token : String  = ""
//    func LoginGoogle(googleDelegate : UIViewController){
//        GIDSignIn.sharedInstance()?.presentingViewController = googleDelegate
//        GIDSignIn.sharedInstance().clientID = Constants.GOOGLE_CLIENT_ID
//        self.delegate =  (googleDelegate as! GoogleLoginDelegate)
//        //Mark : Receive google singin notificaiton
//        NotificationCenter.default.addObserver(self,selector: #selector(self.receiveToggleAuthUINotification(_:)),
//                                               name: NSNotification.Name(rawValue: "GoogleAuthNotification"),
//                                               object: nil)
//        let googleUser =   GIDSignIn.sharedInstance().currentUser
//        if googleUser != nil {
//            access_token = (googleUser?.authentication.accessToken)!
//            delegate?.googleLoginSuccess(user: User().genrateUserFromGoogle(googleUser: googleUser))
//            return
//        }
//        GIDSignIn.sharedInstance().signIn()
//    }
//    
//    func Logout() {
//        GIDSignIn.sharedInstance().signOut()
//    }
//    
//    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
//        if notification.name.rawValue == "GoogleAuthNotification" {
//            if notification.userInfo != nil {
//                guard let userInfo = notification.userInfo as? [String:Any] else { return }
//                if let google_user : GIDGoogleUser = userInfo["user"] as? GIDGoogleUser{
//                    delegate?.googleLoginSuccess(user: User().genrateUserFromGoogle(googleUser: google_user))
//                }else{
//                    delegate?.googleLoginFailed(error: "Unable to login with Google!")
//                }
//            }else{
//                delegate?.googleLoginFailed(error: "Unable to login with Google!")
//            }
//        }
//    }
//}
