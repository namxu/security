//
//  GooglePlacesClass.swift
//  Trecco
//
//  Created by user on 20/11/2019.
//  Copyright © 2019 Jhony. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
//import GoogleMaps
//import Alamofire
//import SwiftyJSON

private let widthKey = "width"
private let heightKey = "height"
private let photoReferenceKey = "photo_reference"

class QPhoto: NSObject {
    
    var width: Int?
    var height: Int?
    var photoRef: String?
    init(photoInfo: [String:Any]) {
        height = photoInfo[heightKey] as? Int
        width = photoInfo[widthKey] as? Int
        photoRef = photoInfo[photoReferenceKey] as? String
    }
//    func getPhotoURL(maxWidth:Int) -> URL? {
//        if let ref = self.photoRef {
//            return APIConstants.googlePhotoURL(photoReference: ref, maxWidth: maxWidth)
//        }
//        return nil
//    }
}

private let geometryKey = "geometry"
private let locationKey = "location"
private let latitudeKey = "lat"
private let longitudeKey = "lng"
private let nameKey = "name"
private let openingHoursKey = "opening_hours"
private let openNowKey = "open_now"
private let vicinityKey = "vicinity"
private let typesKey = "types"
private let photosKey = "photos"

//
struct Periods : Codable {
    let close : Close? = Close()
    let open : Open? = Open()
    
//    case close = "close"
//    case open = "open"

}

struct Open : Codable {
    let day : Int? = 0
    let time : String? = ""

//    case day = "day"
//    case time = "time"

}

struct Close : Codable {
    let day : Int? = 0
    let time : String? = ""

//    case day = "day"
//    case time = "time"

    

}

struct Opening_hours : Codable {
    let open_now : Bool? = false
    let periods : [Periods]? = [Periods]()
    let weekday_text : [String]? = [String]()

//    case open_now = "open_now"
//    case periods = "periods"
//    case weekday_text = "weekday_text"
    
}

//

func convertDictToJson(dict : [[String:Any]]) -> String?
{

    do {
        let jsonData = try JSONSerialization.data(withJSONObject:dict, options:[])
        let jsonDataString = String(data: jsonData, encoding: String.Encoding.utf8)!
        print("Post Request Params : \(jsonDataString)")
        return jsonDataString
    } catch {
        print("JSON serialization failed:  \(error)")
        return ""
    }
    
}


class QPlace: NSObject {
    
    var placeId: String
    var id:String
    var isSelected:Bool = false
    var weekday_text:[String]? = [String]()
    var location: CLLocationCoordinate2D?
    var name: String?
    var periods: [[String:Any]] = [[String:Any]]()
    var photos: [QPhoto]?
    var vicinity: String?
    var isOpen: Bool?
    var types: [String]?
    var phone: String?
    var internationalPhone:String?
//    var marker : GMSMarker?
    var userLocation: CLLocationCoordinate2D?
    var formatted_address:String
    var rating:Double?
    var total_rating:Int?
    var details: [String : Any]?
    var reviews = [ReviewQPlace]()
    var icon:String? = ""
    var website:String?
    var url:String?
    var price_level:Double?
    init(placeInfo:[String: Any]) {
        // id

        placeId = placeInfo["place_id"] as? String ?? ""
        id = placeInfo["id"] as? String ?? ""
        formatted_address = placeInfo["formatted_address"] as? String  ?? ""
        internationalPhone = placeInfo["international_phone_number"] as? String ?? ""
        phone = placeInfo["formatted_phone_number"] as? String ?? ""
        icon = placeInfo["icon"] as? String ?? ""
        website = placeInfo["website"] as? String ?? ""
        url = placeInfo["url"] as? String ?? ""
        price_level = placeInfo["price_level"] as? Double ?? 0.0
        // coordinates
        if let g = placeInfo[geometryKey] as? [String:Any] {
            if let l = g[locationKey] as? [String:Double] {
                if let lat = l[latitudeKey], let lng = l[longitudeKey] {
                    location = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                }
            }
        }
        
        if let reviews = placeInfo["reviews"] as? [[String:Any]]{
            self.reviews.removeAll()
            for item in reviews {
                self.reviews.append(ReviewQPlace.init(placeInfo: item))
            }
        }
        // name
        rating = placeInfo["rating"] as? Double ?? 0.0
        total_rating = placeInfo["user_ratings_total"] as? Int ?? 0
        name = placeInfo[nameKey] as? String
        
        // opening hours
        if let oh = placeInfo[openingHoursKey] as? [String:Any] {
            if let on = oh[openNowKey] as? Bool {
                isOpen = on
            }
            if let weakday = oh["weekday_text"] as? [String] {
                self.weekday_text = weakday
            }
            if let perio = oh["periods"] as? [[String:Any]]{
                periods = perio
            }
        }
        
        // vicinity
        vicinity = placeInfo[vicinityKey] as? String
        
        // types
        types = placeInfo[typesKey] as? [String]

        // photos
        photos = [QPhoto]()
        if let ps = placeInfo[photosKey] as? [[String:Any]] {
            for p in ps {
                photos?.append(QPhoto.init(photoInfo: p))
            }
        }
    }
    
    func getDescription() -> String {
        
        var s : [String] = []
        
        if let name = name {
            s.append("Name: \(name)")
        }
        
        if let vicinity = vicinity {
            s.append("Vicinity: \(vicinity)")
        }
        
        if let types = types {
            s.append("Types: \(types.joined(separator: ", "))")
        }
        
        if let isOpen = isOpen {
            s.append(isOpen ? "OPEN NOW" : "CLOSED NOW")
        }
        let ll = s.joined(separator: "\n")
        
        return ll
    }
    
    func heightForComment(_ font: UIFont, width: CGFloat) -> CGFloat {
        let desc = getDescription()
        let rect = NSString(string: desc).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(rect.height)
    }
    
    func distanceinMiles()->String{
        let latA = Double((userLocation?.latitude) ?? -33.90)
        let logA = Double((userLocation?.longitude) ?? 151.190001)
        let latB = Double((location?.latitude)!)
        let logB = Double((location?.longitude)!)
        let locA = CLLocation(latitude: CLLocationDegrees(latA), longitude: CLLocationDegrees(logA))
        let locB = CLLocation(latitude: CLLocationDegrees(latB), longitude: CLLocationDegrees(logB))
        let distance: CLLocationDistance = locA.distance(from: locB)

        return "\(String(format: "%.1f", distance / 1609.344)) miles away"
    }
    func distanceinMiles(lat:Double,lng:Double)->Double{
        let latA = Double(lat)
        let logA = Double(lng)
        let latB = Double((location?.latitude) ?? 0.0)
        let logB = Double((location?.longitude) ?? 0.0)
        let locA = CLLocation(latitude: CLLocationDegrees(latA), longitude: CLLocationDegrees(logA))
        let locB = CLLocation(latitude: CLLocationDegrees(latB), longitude: CLLocationDegrees(logB))
        let distance: CLLocationDistance = locA.distance(from: locB)
        let disInMiles = distance / 1609.344
        return disInMiles//"\(String(format: "%.1f", )) mil"
    }
    func distanceinMilesDouble(lat:Double,lng:Double)->Double{
        let latA = Double(lat)
        let logA = Double(lng)
        let latB = Double((location?.latitude) ?? 0.0)
        let logB = Double((location?.longitude) ?? 0.0)
        let locA = CLLocation(latitude: CLLocationDegrees(latA), longitude: CLLocationDegrees(logA))
        let locB = CLLocation(latitude: CLLocationDegrees(latB), longitude: CLLocationDegrees(logB))
        let distance: CLLocationDistance = locA.distance(from: locB)
        
        return (distance / 1609.344)
    }
    
}

class ReviewQPlace:NSObject {
    var author_name:String?
    var author_url:String?
    var language:String?
    var profile_photo_url:String?
    var rating:Double?
    var relative_time_description:String?
    var text:String?
    var time : Double?
    init(placeInfo:[String: Any]) {
        author_name = placeInfo["author_name"] as? String ?? ""
        author_url = placeInfo["author_url"] as? String ?? ""
        language = placeInfo["language"] as? String ?? ""
        profile_photo_url = placeInfo["profile_photo_url"] as? String ?? ""
        rating = placeInfo["rating"] as? Double ?? 0.0
        relative_time_description = placeInfo["relative_time_description"] as? String ?? ""
        text = placeInfo["text"] as? String ?? ""
        time = placeInfo["time"] as? Double ?? 0.0
    }
}

struct QPlacesResponse {
    var nextPageToken: String?
    var status: String  = "NOK"
    var places: [QPlace]?
    
    init?(dic:[String : Any]?) {
        nextPageToken = dic?["next_page_token"] as? String
        
        if let status = dic?["status"] as? String {
            self.status = status
        }
        
        if let results = dic?["predictions"] as? [[String : Any]]{
            var places = [QPlace]()
            for place in results {
                places.append(QPlace.init(placeInfo: place))
            }
            self.places = places
        }
    }
    
    func canLoadMore() -> Bool {
        if status == "OK" && nextPageToken != nil && nextPageToken?.count ?? 0 > 0 {
            return true
        }else {
            print("false")
        }
        return false
    }

}

struct QNearbyPlacesResponse {
    var nextPageToken: String?
    var status: String  = "NOK"
    var places: [QPlace]?
    
    init?(dic:[String : Any]?) {
        nextPageToken = dic?["next_page_token"] as? String
        
        if let status = dic?["status"] as? String {
            self.status = status
        }
        
        if let results = dic?["results"] as? [[String : Any]]{
            var places = [QPlace]()
            for place in results {
                places.append(QPlace.init(placeInfo: place))
            }
            self.places = places
        }
    }
    
    func canLoadMore() -> Bool {
        if status == "OK" && nextPageToken != nil && nextPageToken?.count ?? 0 > 0 {
            return true
        }else {
            print("false")
        }
        return false
    }

}

//
//class NearbyPlacesController {
//
//    static func getCategories() -> [QCategory] {
//        let list:[QCategory] = ["Hospitals & clinics","Bakery", "Doctor", "School", "Taxi_stand", "Hair_care", "Restaurant", "Pharmacy", "Atm", "Gym", "Store", "Spa"]
//        return list
//    }
//
//    static let searchApiHost = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
//
//    static let googlePhotosHost = "https://maps.googleapis.com/maps/api/place/photo"
//
//    static let googlePlaceDetailsHost = "https://maps.googleapis.com/maps/api/place/details/json"
//
//    static let googleAddressDetailsHost = "https://maps.googleapis.com/maps/api/geocode/json"
//
//
//    static func getNearbyPlaces(by category:String,lat:String ,lng:String, radius:Int, token: String?, completion: @escaping (QNearbyPlacesResponse?) -> Void) {
//
//        var params : [String : Any]
//
//        if let t = token {
//            params = [
//                "key": Constants.placesKey,
//                "pagetoken" : t,
//            ]
//        } else {
//            params = [
//                "key" :Constants.placesKey,
//                "radius" : radius,
//                "location" : "\(lat),\(lng)",
//                "type" : category.lowercased()
//            ]
//        }
//
//
////        AF.request(searchApiHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////
////            let response = QNearbyPlacesResponse.init(dic: response.result.value as? [String: Any])
////            completion(response)
////        }
//    }
//
//    static func getPlaceDetails(place:String, completion: @escaping (String) -> Void) {
//
//        var params : [String : Any]
//        params = [
//            "key" :Constants.placesKey,
//            "placeid" : place,
//        ]
//
////        AF.request(googlePlaceDetailsHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////            let value = response.result.value as? [String : Any]
////            let items = (value)?["result"] as? [String : Any]
////            if let phone = items?["international_phone_number"] as? String {
////                completion(phone.replaceSpace(str: "").replacingOccurrences(of: "+", with: ""))
////            }else {
////                completion("")
////            }
////
////        }
//    }
//
//    static func getPlaceReviewsFull(id:String,name:String, completion: @escaping (QPlace) -> Void) {
//
////        var params : [String : Any]
////        params = [
////            "key" : AppDelegate.googlePlacesAPIKey,
////            "placeid" : place,
////        ]
//        //GET
//        let link = "https://mybusiness.googleapis.com/v4/accounts/id/locations/name/reviews"
////        AF.request(link, method: .get, parameters: nil, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////            let value = response.result.value as? [String : Any]
////            let items = (value)?["result"] as? [String : Any]
////            if(items != nil){
////                completion(QPlace.init(placeInfo: items!))
////            }else {
////                let place = QPlace(placeInfo: [String : Any]())
////                completion(place)
////            }
////        }
////        Alamofire.request(googlePlaceDetailsHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////            let value = response.result.value as? [String : Any]
////            let items = (value)?["result"] as? [String : Any]
////            if(items != nil){
////                completion(QPlace.init(placeInfo: items!))
////            }else {
////                let place = QPlace(placeInfo: [String : Any]())
////                completion(place)
////            }
////
////
////
////        }
//    }
//    static func getPlaceAddress(lat:Double,lng:Double,place:String, completion: @escaping (QPlace?) -> Void) {
//            var params : [String : Any]
//            params = [
//                "latlng"  : "\(lat),\(lng)",
//                "key" :Constants.placesKey
//            ]
////            AF.request(googleAddressDetailsHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////                let value = response.result.value as? [String : Any]
////                let items = (value)?["result"] as? [String : Any]
////                if(items != nil){
////                    completion(QPlace.init(placeInfo: items!))
////                }else {
////    //                let place = QPlace(placeInfo: [String : Any]())
////                    completion(nil)
////                }
////            }
//        }
//    static func getPlaceDetailsFull(place:String, completion: @escaping (QPlace?) -> Void) {
//        var params : [String : Any]
//        params = [
//            "key" :Constants.placesKey,
//            "placeid" : place,
//        ]
////        AF.request(googlePlaceDetailsHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////            let value = response.result.value as? [String : Any]
////            let items = (value)?["result"] as? [String : Any]
////            if(items != nil){
////                completion(QPlace.init(placeInfo: items!))
////            }else {
//////                let place = QPlace(placeInfo: [String : Any]())
////                completion(nil)
////            }
////        }
//    }
//
//    static func getPlaceDetailsUpdate(place:QPlace, completion: @escaping (QPlace) -> Void) {
//
//        var params : [String : Any]
//        params = [
//            "key" : Constants.placesKey,
//            "placeid" : place.placeId,
//        ]
//
////        AF.request(googlePlaceDetailsHost, parameters: params, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
////            let value = response.result.value as? [String : Any]
////            place.details = (value)?["result"] as? [String : Any]
////            if let phone = place.details?["international_phone_number"] as? String {
////                place.phone =  phone
////            }else {
////                place.phone =  ""
////            }
////            completion(place)
////        }
//    }
//
//    static func googlePhotoURL(photoReference:String, maxWidth:Int) -> URL? {
//        return URL.init(string: "\(googlePhotosHost)?maxwidth=\(maxWidth)&key=\(Constants.placesKey)&photoreference=\(photoReference)")
//    }
//}
//


struct QCategory {
    var name:String
    init(name:String) {
        self.name = name
    }
}

extension QCategory: ExpressibleByStringLiteral {
    init(stringLiteral value: String) {
        self.name = value
    }
    init(unicodeScalarLiteral value: String) {
        self.init(name: value)
    }
    init(extendedGraphemeClusterLiteral value: String) {
        self.init(name: value)
    }
}
