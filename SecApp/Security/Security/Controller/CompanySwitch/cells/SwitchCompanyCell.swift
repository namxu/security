//
//  SwitchCompanyCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 30/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SwitchCompanyCell: UITableViewCell {

    static let identifier = "SwitchCompanyCell"
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var uiCheck:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(title:String,isSelected:Bool){
        self.lblTitle.text = title
        self.uiCheck.isHidden = !isSelected
    }
    
}
