//
//  DescriptionVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 19/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class DescriptionVC:BaseViewController {
    
    static let identifier = "DescriptionVC"
    @IBOutlet weak var lblRes:UILabel!
    @IBOutlet weak var lblDuties:UILabel!
    @IBOutlet weak var mapView:GMSMapView!
    
    @IBOutlet weak var uiJobTitle:UIView!
    @IBOutlet weak var uiJobDescripiton:UIView!
    @IBOutlet weak var uiBarCode:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadMap()
        if(DataManager.sharedInstance.isAdmin()){
            self.uiBarCode.isHidden = false
            self.uiJobTitle.isHidden = true
            self.uiJobDescripiton.isHidden = true
        }else{
            self.uiBarCode.isHidden = true
            self.uiJobTitle.isHidden = false
            self.uiJobDescripiton.isHidden = false
        }
    }
    
    func loadMap(){
//        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
    }
    
    @IBAction func onClickReport(_ sender:UIButton){
        let vc = self.getVC(storyboard: .MAIN, vcIdentifier: SiteReportVC.identifier)
        self.presentVC(vc)
    }
    
    @IBAction func onClickCheckIn(_ sender:UIButton){
        
    }
    
}
