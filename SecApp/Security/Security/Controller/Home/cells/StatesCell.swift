//
//  StatesCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 19/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class StatesCell: UITableViewCell {

    static let identifier = "StatesCell"
    @IBOutlet weak var ivImage:UIImageView!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var btnState:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(_ item:StateItem){
        self.lblDescription.text = item.description
        self.btnState.setTitle(item.buttonState.rawValue, for: .normal)
//        self.btnState.setTitleColor(UIColor.init(hexColor: item.buttonStateColor.rawValue), for: .normal)
//        self.btnState.borderColor = UIColor.init(hexColor: item.buttonStateColor.rawValue)
//        self.btnState.borderWidth = CGFloat(1)
//        self.ivImage.image = UIImage.init(named: item.pic)
    }
    
}


class StateItem:Codable {
    var pic:String = ""
    var description:String = ""
    var buttonState:ButtonState = .ACTIVE
    var buttonStateColor:ButtonStateColot = .ACTIVE
    
    init() {
        
    }
    init(_ pic:String,_ description:String,_ buttonState:ButtonState,_ buttonStateColor:ButtonStateColot) {
        self.pic = pic
        self.description = description
        self.buttonStateColor = buttonStateColor
        self.buttonState = buttonState
    }
}
enum ButtonState :String,Codable {
    case ACTIVE = "Active"
    case PENDING = "Pending"
    case COMPLETE = "Complete"
}
enum ButtonStateColot :String,Codable {
    case ACTIVE = "2B8BB1"
    case PENDING = "E2CB82"
    case COMPLETE = "659A68"
}
