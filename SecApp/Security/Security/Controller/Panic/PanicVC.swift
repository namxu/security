//
//  PanicVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 22/08/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class PanicVC: BaseViewController {

    static let identifier = "PanicVC"
    
    @IBOutlet weak var btnCall911:UIButton!
    @IBOutlet weak var btnCallManager:UIButton!
    @IBOutlet weak var btnCallSupervisor:UIButton!
    @IBOutlet weak var btnOkThanks:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onClick911(_ sd:UIButton){
        sd.onDialNumber("911")
    }
    
    @IBAction func onClickManager(_ sd:UIButton){
        sd.onDialNumber("911")
    }
    
    @IBAction func onClickSupervisor(_ sd:UIButton){
        sd.onDialNumber("9113427325")
    }
    
    @IBAction func onClickOkThanks(_ sd:UIButton){
        self.dismiss(animated: true, completion: {
            
        })
    }
    

}

extension UIView {
    func onDialNumber(_ PhoneNumber : String){
        var phone = PhoneNumber.replacingOccurrences(of: "(", with: "")
        phone = phone.replacingOccurrences(of: ")", with: "")
        phone = phone.replacingOccurrences(of: " ", with: "")
        phone = phone.replacingOccurrences(of: "-", with: "")
        //001
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }else{
            
            //            self.showTreccoToastAlert(title: "Error", text: "Invalid Phone Number!")
        }
    }
}
