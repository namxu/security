//
//  BaseVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 13/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Kingfisher
import KingfisherWebP
import Loaf
import AVFoundation
import YPImagePicker
import Toast_Swift


//import MapKit

class BaseViewController: UIViewController  {//,  CLLocationManagerDelegate
    
    
    //MARK: Constraint Keys
    @IBOutlet weak var backImgHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var backImgWidthConstraint:NSLayoutConstraint!
    
    //MARK: Delay - Setup Top Bar White Or Black Icon
    var isNeedToDark = false
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
            if(isNeedToDark){
                return .lightContent//.darkContent
            }else{
                return .default
            }
        } else {
            if(isNeedToDark){
                return .lightContent
            }else{
                return .default
            }
            // Fallback on earlier versions
        }
    }
    
    func setupBettryIconBlackOrWhite(isBlack:Bool = true){
        self.isNeedToDark = isBlack
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
//        KingfisherManager.shared.cache.clearMemoryCache()
//        KingfisherManager.shared.cache.clearDiskCache()
//        KingfisherManager.shared.cache.cleanExpiredDiskCache()
    }
    
    @IBOutlet weak var lblTitleHeader:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.locationManager.requestAlwaysAuthorization()
//        if CLLocationManager.locationServicesEnabled() {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
//            locationManager.startUpdatingLocation()
//        }
        if(self.backImgHeightConstraint != nil && self.backImgWidthConstraint != nil){
            self.backImgHeightConstraint.constant = 28
            self.backImgWidthConstraint.constant = 28
        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.appCameFromBackGround), name: UIApplication.didBecomeActiveNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    @objc func appMovedToBackground() {
        print("App moved to background!")
    }
    
    @objc func appCameFromBackGround(){
        print("App moved to foreground!")
//        VibrateFrenzy.vibrate(for: 5)
        //        FlashFrenzy.flash(for: 2)
    }
    
    
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        guard let _: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//    }
//
//    let locationManager = CLLocationManager()
    //MARK: Delay
    func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    //MARK: Open Place picker Controller
    
    @IBAction func onClickDismiss(_ sender : Any){
        if let nev = self.navigationController{
            nev.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: Set multo Color text
    func  setAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UILabel , color : UIColor, attFont:UIFont) {
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for objStr in attributedStringsArray {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range1)
        }
        lbl.attributedText = attributedString1
    }
    

    func setAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UILabel , color : [UIColor], attFont:[UIFont]) {
        
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for (index,objStr) in attributedStringsArray.enumerated() {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont[index]]
            attributedString1.addAttributes(attribute_font, range:  range1)
            
            attributedString1.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color[index], range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.strokeColor, value: UIColor.white, range: range1)
            attributedString1.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range1)
        }
        lbl.attributedText = attributedString1
    }

    
    
    
}

extension UIViewController {
    
    func openCalendar(delegate: CalendarDateRangePickerViewControllerDelegate){
        let dateRangePickerViewController = CalendarDateRangePickerViewController(collectionViewLayout: UICollectionViewFlowLayout())
        dateRangePickerViewController.delegate = delegate
        dateRangePickerViewController.minimumDate = Date()
        dateRangePickerViewController.maximumDate = Calendar.current.date(byAdding: .year, value: 200, to: Date())
        dateRangePickerViewController.selectedStartDate = Date()
        dateRangePickerViewController.selectedEndDate = Calendar.current.date(byAdding: .day, value: 10, to: Date())
        dateRangePickerViewController.selectedColor = UIColor.red
        dateRangePickerViewController.titleText = "Select Date From To"
        let navigationController = UINavigationController(rootViewController: dateRangePickerViewController)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        self.presentVC(navigationController)
    }
    
    func pushVC(_ vc:UIViewController,_ isAnimate:Bool = true){
        self.navigationController?.pushViewController(vc, animated: isAnimate)
    }
    
    func presentVC(_ vc:UIViewController,_ isAnimate:Bool = true){
        self.present(vc, animated: isAnimate) {
            
        }
    }
}

struct TreccoColor{
    static let placeHolderTf = UIColor.init(hexColor: "C7C7CD")
    static let redColor = UIColor.init(hexColor: "FB3E46")//EE2157
    static let gradientLeftColor = UIColor.init(hexColor: "3AE5A5")
    static let gradientRightColor = UIColor.init(hexColor: "1CDAE9")
    static let unselectColorTripList = UIColor.init(hexColor: "D8D8D8")
    static let searchTagUnSelectColorNew = UIColor.init(hexColor: "EEEFF2")
    static let yellowColor = UIColor.init(hexColor: "FFDD1B")
    static let purpleColor = UIColor.init(hexColor: "3F1551")
    static let greenColor = UIColor.init(hexColor: "8CC63F")//"8CC63F"
    static let purpleDarkColor = UIColor.init(hexColor: "1C0E44")
    static let segmentUnSelectColor = UIColor.init(hexColor: "BEBEBE")
    static let offSwtichThumbColor = UIColor.init(hexColor: "DFDFDF")
    static let grayKindColor = UIColor.init(hexColor: "8A8A8A")
    static let blackKindColor = UIColor.init(hexColor: "313131")
    static let white = UIColor.init(hexColor: "ffffff")
    static let frozyKindColor = UIColor.init(hexColor: "02BDC4")
    static let nonSelectColorContentBy = UIColor.init(hexColor: "B5B8BB")
    static let selectColorContentBy = UIColor.init(hexColor: "24375C")
    static let treccoBlue = UIColor.init(hexColor: "2F2F2F")//old color "24375C"
    static let homeSubTitleColor = UIColor.init(hexColor: "939393")
    static let treccoBlueOld = UIColor.init(hexColor: "24375C")//old color "24375C"
    static let messageArrowCountZeroColor = UIColor.init(hexColor: "D1D8E7")
    static let lineColor = UIColor.init(hexColor: "F3F3F3")//D8D8D8
    static let searchNonSelectColor = UIColor.init(hexColor: "9AABB6")
    // MARK: New Font Color
    static let textColor = UIColor.init(hexColor: "2F2F2F")
    static let calenderSelected = UIColor.init(hexColor: "24375C")//UIColor.init(hexColor: "01B789")
    static let dateNumberColor = UIColor.init(hexColor: "171F24")
    static let monthColor = UIColor.init(hexColor: "24375C")
    static let deleteSwipeColor = UIColor.init(hexColor: "FEEBF1")
    static let deletrSwipeTxtColor = UIColor.init(hexColor: "F51E5B")
    static let unreadNotifyColor = UIColor.init(hexColor: "F6F7F9")
    static let eventDetailAllHotelNoteSelection = UIColor.init(hexColor: "4A90E2")
    static let onlyPicterCounterColor = UIColor.init(hexColor: "393939")
    static let protipLikeColor = UIColor.init(hexColor: "F54B64")
    static let protipLikeColorBg = UIColor.init(hexColor: "FEEDEF")
    static let protipEditColorBg = UIColor.init(hexColor: "F4F4F6")
    static let calenderDayWeekColor = UIColor.init(hexColor: "778087")
    static let multiDaysUnSelectColor = UIColor.init(hexColor: "ECECEC")
    static let searchTagUnSelectColor = UIColor.init(hexColor: "D0D4DA")
    static let publicPrivateUnSelectColor = UIColor.init(hexColor: "676767")
    static let proTripAddedInWhichColor = UIColor.init(hexColor: "979797")
    static let culesterStrokeColor = UIColor.init(hexColor: "182A4D")
    static let mapBoxLayerColor = UIColor.init(hexColor: "598CD8")
    static let txtMsgColor = UIColor.init(hexColor: "97A1A4")//UIColor.white
    static let chatBgColor = UIColor.init(hexColor: "FBFCFE")
    static let culesterBGColor = UIColor.init(hexColor: "F8E71C")
    static let toastBgColor = UIColor.init(hexColor: "1FB892")
    static let acceptColor = UIColor.init(hexColor: "E9FBF7")
    
    //
}


extension UIViewController {
    
    var window : UIWindow {
        return UIApplication.shared.windows.first!
    }

    //MARK: Get VC From Storyborad
    func getVC(storyboard : Storyboards, vcIdentifier : String) -> UIViewController {
        //String = kStoryBoardMain
        return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(withIdentifier: vcIdentifier)
    }
    
    func showToast(text:String,type:Loaf.State,location:Loaf.Location,_ duration:Loaf.Duration = .average,_ completation: (() -> ())? = nil){
        switch type {
            
        case .success:
            self.showToastWithImage(text: text, isError: false,location:location)
            completation?()
            break
        case .error:
            self.showToastWithImage(text: text, image: nil, isError: true,location:location)
            completation?()
            break
        case .warning:
            self.showToastWithImage(text: text, image: nil, isError: true,location:location)
            completation?()
            break
        case .info:
            self.showToastWithImage(text: text, image: nil, isError: true,location:location)
            completation?()
            break
        case .custom(_):
            completation?()
            break
            
        }
        
//        Loaf(text, state: type,location: location, sender: self).show(duration) { (dismiss) in
//            completation?()
//        }
        
    }
    
    func showToastWithImage(text:String,image:UIImage? = UIImage.init(named: "RecAddIcon"),isError:Bool = false,location:Loaf.Location = .top) {
        //        ToastStyle = ToastManager.shared.style
        var style = ToastStyle()
        if(isError){
            style.backgroundColor = TreccoColor.redColor.withAlphaComponent(0.8)
            style.displayShadow = true
//            style.messageFont = TreccoFont.kRobotoCondensedRegular(CGFloat(14)).font()
//            style.titleFont = TreccoFont.kRobotoCondensedBold(CGFloat(14)).font()
            style.imageSize = CGSize.init(width: 20, height: 20)
            self.window.makeToast(text, duration: 2.0, position: location == .top ? .top : .bottom, title: nil, image: nil,style:style)
        }else{
            style.backgroundColor = TreccoColor.toastBgColor.withAlphaComponent(0.8)
            style.displayShadow = true
//            style.messageFont = TreccoFont.kRobotoCondensedRegular(CGFloat(14)).font()
//            style.titleFont = TreccoFont.kRobotoCondensedBold(CGFloat(14)).font()
            style.imageSize = CGSize.init(width: 20, height: 20)
            self.window.makeToast(text, duration: 2.0, position: location == .top ? .top : .bottom, title: nil, image: image,style:style)
        }
        
        
    }
    
    var screenWidth:CGFloat {
        get {
            return UIScreen.main.bounds.width
        }
    }
    
    var screenHeight:CGFloat {
        get {
            return UIScreen.main.bounds.height
        }
    }
    
    func getImagePicker(_ count :Int = 1,isMultiSelectin:Bool = false,isAlsoVideo :Bool = false,callBack: @escaping ([YPMediaItem]?,String) -> Void){
        var imagePicker :YPImagePicker?
        //        if(imagePicker == nil){
        var config = YPImagePickerConfiguration()
        config.isScrollToChangeModesEnabled = true
        //            config.onlySquareImagesFromCamera = false
        //            config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.showsVideoTrimmer = true
        config.maxCameraZoomFactor = 1
        config.shouldSaveNewPicturesToAlbum = true
        config.albumName = "Trecco"
        config.showsCrop = .none
        if(isAlsoVideo){
            config.screens = [.library, .photo, .video]
            config.video.compression = AVAssetExportPresetHighestQuality
            config.video.fileType = .mp4
            config.video.recordingTimeLimit = 60.0
            config.video.libraryTimeLimit = 60.0
            config.video.minimumTimeLimit = 3.0
            config.video.trimmerMaxDuration = 60.0
            config.video.trimmerMinDuration = 3.0
        }else{
            config.screens = [.library, .photo]
        }
        config.startOnScreen = YPPickerScreen.photo
        
        config.targetImageSize = YPImageSize.original
        config.overlayView = UIView()
        config.hidesStatusBar = false
        config.hidesBottomBar = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = TreccoColor.treccoBlue
        config.bottomMenuItemUnSelectedTextColour = TreccoColor.nonSelectColorContentBy
        //            config.filters = [DefaultYPFilters.]
        //MARK: Library
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        config.library.mediaType = YPlibraryMediaType.photo
        config.library.defaultMultipleSelection = isMultiSelectin
        config.library.maxNumberOfItems = count
        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        //            config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        YPImagePickerConfiguration.shared = config
        if(imagePicker == nil){
            imagePicker = YPImagePicker(configuration: config)
            //
            //        }
            imagePicker?.didFinishPicking { [unowned imagePicker] items, cancelled in
                if cancelled {
                    callBack(nil,"")
                    print("Picker was canceled")
                }else{
                    if let photo = items.singlePhoto {
                        print(photo.fromCamera) // Image source (camera or library)
                        print(photo.image) // Final image selected by the user
                        print(photo.originalImage) // original image selected by the user, unfiltered
                        print(photo.modifiedImage as Any) // Transformed image, can be nil
                        print(photo.exifMeta as Any) // Print exif meta data of original image.
                        let imageUrl = items.singlePhoto?.image.fixedOrientation()?.writeImageToTemporaryDirectory(resourceName: "image_\(ProcessInfo.processInfo.globallyUniqueString)", fileExtension: "png")
                        callBack(items,(imageUrl?.absoluteString ?? ""))
                    }else if let video = items.singleVideo{
                        print(video.fromCamera)
                        print(video.thumbnail)
                        print(video.url)
                        let imageUrl = items.singleVideo?.thumbnail.fixedOrientation()?.writeImageToTemporaryDirectory(resourceName: "image_\(ProcessInfo.processInfo.globallyUniqueString)", fileExtension: "png")
                        callBack(items,(imageUrl?.absoluteString ?? ""))
                    }
                    
                }
                imagePicker?.dismiss(animated: true, completion: nil)
            }
        }
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    
}

