//
//  ForgetPasswordVerificationSignInVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 13/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class ForgetPasswordVerificationSignInVC: BaseViewController {
    static let identifier = "ForgetPasswordVerificationSignInVC"
    @IBOutlet weak var tfEmail:UITextField!
    @IBOutlet weak var tfPassCode:UITextField!
    @IBOutlet weak var tfPassword:UITextField!
    var email:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tfEmail.text = email
    }
    @IBAction func onClickDone(_ sender:UIButton){
        if(self.tfEmail.text!.isValidEmail() && !self.tfPassCode.text!.isEmpty){
            for items in self.navigationController?.viewControllers ?? [UIViewController]() {
                if(items.isKind(of: SignInVC.self)){
                    self.navigationController?.popToViewController(items, animated: true)
                    break
                }
            }
        }else{
            if(!self.tfEmail.text!.isValidEmail()){
                self.showToast(text: "Invalid Email!", type: .error, location: .top)
                return
            }
            
            if(self.tfPassCode.text!.isEmpty){
                self.showToast(text: "Confirmation Code Required!", type: .error, location: .top)
                return
            }
            if(self.tfPassword.text!.isEmpty){
                self.showToast(text: "Password Required!", type: .error, location: .top)
                return
            }
        }
    }
    
}
