//
//  SliderView.swift
//  Security
//
//  Created by Namxu Ihseruq on 19/04/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import Foundation

import UIKit

class SliderVC: UISimpleSlidingTabController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI(){
        view.backgroundColor = .white
        self.navigationController?.isNavigationBarHidden = true
        // slidingTab
        if(DataManager.sharedInstance.isAdmin()){
            addItem(item: SliderContentVC(), title: "Active") // add first item
            addItem(item: SliderContentVC(), title: "Assigned") // add second item
            addItem(item: SliderContentVC(), title: "Not\nAssigned") // add third item
//            addItem(item: SliderContentVC(), title: "All") // add third item
        }else{
            addItem(item: SliderContentVC(), title: "Active") // add first item
//            addItem(item: SliderContentVC(), title: "Last 24\nHours") // add second item
            addItem(item: SliderContentVC(), title: "Last 30\nDays") // add third item
//            addItem(item: SliderContentVC(), title: "All") // add third item
        }
        setHeaderActiveColor(color: .white) // default blue
        setHeaderInActiveColor(color: .black) // default gray
        setHeaderBackgroundColor(color: .white) // default white
        setHeaderLine(color: .red)
        setCurrentPosition(position: 1) // default 0
        setStyle(style: .fixed) // default fixed
        build() // build
    }
    
}
