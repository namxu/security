//
//  SummeryHeaderCell.swift
//  Security
//
//  Created by Namxu Ihseruq on 22/08/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SummeryHeaderCell: UITableViewCell {

    static let identifier = "SummeryHeaderCell"
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var ivIndicator:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var onHeaderClick:(() -> Void)?
    @IBAction func onClickCell(_ sd:UIButton){
        self.onHeaderClick?()
    }
    
}
