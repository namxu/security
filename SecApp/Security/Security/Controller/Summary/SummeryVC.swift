//
//  SummeryVC.swift
//  Security
//
//  Created by Namxu Ihseruq on 22/08/2020.
//  Copyright © 2020 mac. All rights reserved.
//

import UIKit

class SummeryVC: BaseViewController {

    static let identifier = "SummeryVC"
    @IBOutlet weak var tableView:UITableView!
    var mData = SummeryModel.getData()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTbl()
    }
    


}

extension SummeryVC : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = self.mData[section]
        return item.isOpened ? item.list.count : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mData.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SummeryDescriptionCell.identifier, for: indexPath) as! SummeryDescriptionCell
        cell.lblDescription.numberOfLines = 0
        setAttributedTextForLabel(mainString: "site id here\nJob\(indexPath.item + 1)\nMonday 1, (Month Name)\nStart Time - End Time", attributedStringsArray: ["site id here","Job\(indexPath.item + 1)","Monday 1, (Month Name)","Start Time - End Time"], lbl: cell.lblDescription, color: [UIColor.lightGray,UIColor.darkGray,UIColor.lightGray,UIColor.lightGray], attFont: [BeboFont.light(CGFloat(14)).font(),BeboFont.medium(CGFloat(14)).font(),BeboFont.light(CGFloat(14)).font(),BeboFont.light(CGFloat(14)).font()])
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let item = self.mData[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: SummeryHeaderCell.identifier) as! SummeryHeaderCell
        cell.lblTitle.text = item.header
        if(item.isOpened){
            cell.ivIndicator.transform = cell.ivIndicator.transform.rotated(by: .pi)        // 180˚
        }else{
            cell.ivIndicator.transform = cell.ivIndicator.transform.rotated(by: 0)  // 270˚
        }
        cell.onHeaderClick = {
            self.mData[section].isOpened = !self.mData[section].isOpened
            self.tableView.reloadData()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(45)
    }
    
    private func setupTbl(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.contentInset = UIEdgeInsets.init(top: CGFloat(8), left: CGFloat(0), bottom: CGFloat(8), right: CGFloat(0))
        self.tableView.register(UINib.init(nibName: SummeryHeaderCell.identifier, bundle: nil), forCellReuseIdentifier: SummeryHeaderCell.identifier)
        self.tableView.register(UINib.init(nibName: SummeryDescriptionCell.identifier, bundle: nil), forCellReuseIdentifier: SummeryDescriptionCell.identifier)
        
    }
    
}


class SummeryModel:Codable{
    
    var header:String = ""
    var list:[String] = [String]()
    var isOpened:Bool = false
    
    static func getData() -> [SummeryModel]{
        var list = [SummeryModel]()
        for item in 0...3 {
            let itemSummery = SummeryModel()
            itemSummery.list.append(contentsOf: SummeryModel.getStringData())
            switch item{
            case 0 :
                itemSummery.header = "January 2020"
                break
            case 1 :
                itemSummery.header = "February 2020"
                break
            case 2 :
                itemSummery.header = "March 2020"
                break
            case 3 :
                itemSummery.header = "April 2020"
                break
            default : break
            }
            
            list.append(itemSummery)
        }
        return list
    }
    
    static func getStringData() -> [String]{
        var list = [String]()
        list.append("")
        list.append("")
        list.append("")
        list.append("")
        list.append("")
        return list
    }
}

extension UIImage {
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.y, y: -origin.x,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return rotatedImage ?? self
        }

        return self
    }
}
