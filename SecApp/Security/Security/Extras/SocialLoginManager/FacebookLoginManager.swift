//
//  FacebookLoginManager.swift
//  Trecco
//
//  Created by CP on 11/12/19.
//  Copyright © 2019 Jhony. All rights reserved.
//
//
//import UIKit
//import FacebookLogin
//import FacebookCore
//import FacebookCore
//import GoogleSignIn
//
//protocol FacebookLoginDelegate {                    // codingpixel.test1@gmail.com
//    func facebookLoginSuccess(user : User)
//    func facebookLoginFailed(error : String)
//}
//
//protocol FacebookFriendsListDelegate {
//    func FacebookFriends(list : [[String : Any]])
//}
//class FacebookLoginManager: NSObject {
//    static let sharedInstance = FacebookLoginManager()
//    var fbdelegate : FacebookLoginDelegate?
//    var fbdelegateFriends : FacebookFriendsListDelegate?
//    var access_token : String  = ""
//    
//    func loginWithFacebook (facebookDelegate : UIViewController) {
//        let user = User()
//        self.fbdelegate =  (facebookDelegate as! FacebookLoginDelegate)
//        let loginManager = LoginManager()
//        loginManager.logOut()
//        loginManager.logIn(permissions: [ .publicProfile,.email], viewController: facebookDelegate) { loginResult in
//            switch loginResult {
//            case .failed(let error):
//                print(error)
//                self.fbdelegate?.facebookLoginFailed(error: error.localizedDescription ?? "")
//            case .cancelled:
//                self.fbdelegate?.facebookLoginFailed(error: "User cancelled login.")
//            case .success( _,  _, let accessToken):
//                let req = GraphRequest(graphPath: "me", parameters: ["fields":"email,id,name,picture"], tokenString: accessToken.tokenString, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
//                
//                req.start(completionHandler: { (Connection, result, error) in
//                    if(error == nil){
//                        print("result \(String(describing: result))")
//                        if let response = result as? [String:Any] {
//                            user.email = response["email"] as? String ?? ""
//                            user.token = response["id"] as? String ?? ""
//                            user.first_name = response["name"] as? String ?? ""
//                            user.profile_photo = "http://graph.facebook.com/\((response["id"] as! String))/picture?type=large"
//                            self.fbdelegate?.facebookLoginSuccess(user: user)
//                            
//                        }else{
//                            self.fbdelegate?.facebookLoginFailed(error: "Facebook Error!")
//                        }
//                    }
//                    else
//                    {
//                        print("error \(String(describing: error))")
//                        self.fbdelegate?.facebookLoginFailed(error: error as! String)
//                    }
//                })
//                print("Logged in!")
//            }
//        }
//    }
//    
//    
//    func getFacebookFriendsList(fbFriendsDelegate  : UIViewController) {
//        self.fbdelegateFriends =  (fbFriendsDelegate as! FacebookFriendsListDelegate)
//        if let fb_tocken = AccessToken.current{
//            print("already login \(fb_tocken.tokenString)")
//            let req = GraphRequest(graphPath: "me/friends", parameters: ["fields": "id, first_name"], tokenString: fb_tocken.tokenString, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
//            req.start(completionHandler: { (Connection, result, error) in
//                if(error == nil)
//                {
//                    print("result \(String(describing: result))")
//                    self.fbdelegateFriends?.FacebookFriends(list: [["Result":result as Any]])
//                }
//                else
//                {
//                    print("error \(String(describing: error))")
//                    self.fbdelegate?.facebookLoginFailed(error: error as! String)
//                }
//            })
//        }else{
//            let loginManager = LoginManager()
//            loginManager.logOut()
//            loginManager.logIn(permissions: [ .publicProfile,.email], viewController: fbFriendsDelegate) { loginResult in
//                switch loginResult {
//                case .failed(let error):
//                    print(error)
//                    self.fbdelegate?.facebookLoginFailed(error: error.localizedDescription ?? "")
//                case .cancelled:
//                    self.fbdelegate?.facebookLoginFailed(error: "User cancelled login.")
//                    print("User cancelled login.")
//                case .success( _,  _, let accessToken):
//                    let req = GraphRequest(graphPath: "me/friends", parameters: ["fields": "id, first_name, last_name, middle_name, name, email, picture"], tokenString: accessToken.tokenString, version: nil, httpMethod: HTTPMethod(rawValue: "GET"))
//                    
//                    req.start(completionHandler: { (Connection, result, error) in
//                        if(error == nil)
//                        {
//                            print("result \(String(describing: result))")
//                            self.fbdelegateFriends?.FacebookFriends(list: [["Result":result as Any]])
//                        }
//                        else
//                        {
//                            print("error \(String(describing: error))")
//                            self.fbdelegate?.facebookLoginFailed(error: error as! String)
//                        }
//                    })
//                    break
//                    
//                }
//            }
//        }
//        
//    }
//}
